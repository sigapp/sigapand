package com.sigapp.id;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sigapp.id.Adapter.TahapanItemAdapter;
import com.sigapp.id.App.AppController;
import com.sigapp.id.App.SigapApp;
import com.sigapp.id.Data.TahapanItem;
import com.twotoasters.jazzylistview.JazzyListView;
import com.twotoasters.jazzylistview.effects.TiltEffect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TahapActivity extends AppCompatActivity
{
    private JazzyListView LVTahapan;
    private TahapanItemAdapter listAdapter;
    private TextView LKeterangan;
    private List<TahapanItem> items;
    private String URL;

    public void refreshContent()
    {
        Log.i("HERE ", "refreshContent: " + URL);
        JsonObjectRequest jsonReq2 = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                if (response != null)
                {
                    parse(response);
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (!isFinishing())
                {
                    refreshContent();
                }
            }
        });

        AppController.getInstance().addToRequestQueue(jsonReq2);
    }

    private void parse(JSONObject response) {
        try
        {
            LKeterangan.setVisibility(View.GONE);
            JSONArray feedArray = response.getJSONArray("tahapan");

            items.clear();

            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                TahapanItem item = new TahapanItem();
                item.setTahap(feedObj.getString("tahap"));
                item.setWaktuambil(feedObj.getString("waktuambil"));
                item.setStatus(feedObj.getString("status"));
                item.setNamatahap(feedObj.getString("namatahap"));
                item.setWaktu(feedObj.getString("waktu"));
                item.setSisawaktu(feedObj.getString("sisawaktu"));
                item.setReward(feedObj.getString("reward"));
                item.setKeterangan(feedObj.getString("keterangan"));
                item.setTersedia(feedObj.getString("tersedia"));
                item.setPoint(feedObj.getString("point"));

                if (feedObj.getString("tahap").equalsIgnoreCase("DONE"))
                {
                    item.setSelesai(true);
                    items.add(0, item);
                }
                else
                {
                    item.setSelesai(false);
                    items.add(item);
                }
            }

            listAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            LKeterangan.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tahap);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        LVTahapan = (JazzyListView) findViewById(R.id.LVTahapan);
        LKeterangan = (TextView) findViewById(R.id.LKeterangan);
        LVTahapan.setTransitionEffect(new TiltEffect());

        items = new ArrayList<TahapanItem>();
        listAdapter = new TahapanItemAdapter(this, items);
        LVTahapan.setAdapter(listAdapter);

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(500);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        LVTahapan.setLayoutAnimation(controller);

        URL = getString(R.string.alamat_server) + getString(R.string.link_ambil_tahapan) + "?pgid=" + SigapApp.getPgid();
    }

    @Override
    protected void onResume()
    {
        refreshContent();
        super.onResume();
    }
}
