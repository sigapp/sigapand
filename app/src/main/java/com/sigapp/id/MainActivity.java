package com.sigapp.id;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.sigapp.id.App.Animasi;
import com.sigapp.id.App.AppController;
import com.sigapp.id.App.Crypt;
import com.sigapp.id.App.EditTextDrawerItem;
import com.sigapp.id.App.EditTextPassDrawerItem;
import com.sigapp.id.App.LaporanImageView;
import com.sigapp.id.App.RequestHandler;
import com.sigapp.id.App.SigapApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity
{
    private GestureDetectorCompat gestureDetectorCompat;
    private Toolbar toolbar;
    private View HeaderLayout;
    private ImageButton BPlayEbooks;
    private PrimaryDrawerItem IM_EBooks, JM_Setting, IM_Profile, IM_ChangePass, IM_Logout, IM_Login, IM_Save, IM_BangunDesa;
    private EditTextDrawerItem EM_Nama, EM_Email, EM_TTL, EM_Alamat, EM_Lembaga;
    private EditTextPassDrawerItem EM_OldPass, EM_NewPass, EM_Confirm;
    private ImageView BBack, IVSigap;
    private LaporanImageView IVFoto;
    private TextView LNama;
    private LinearLayout LLProfile;
    private Drawer main_drawer;
    private boolean first = true;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    private String dnama, demail, dttl, dalamat, dorganisasi, dpassword, dfoto;

    private int halaman_nav = 0;

    public String[] AmbilIsian(int bagian)
    {
        String[] Isian = {"", "", "", "", "", "", ""};

        if (bagian == 1)
        {
            Isian[0] = SigapApp.getPgid();
            Isian[1] = EM_Nama.getTeks().toString();
            Isian[2] = EM_Email.getTeks().toString();
            Isian[3] = EM_TTL.getTeks().toString();
            Isian[4] = EM_Alamat.getTeks().toString();
            Isian[5] = EM_Lembaga.getTeks().toString();
            Isian[6] = dpassword;
        }
        else
        {
            Isian[0] = SigapApp.getPgid();
            Isian[1] = dnama;
            Isian[2] = demail;
            Isian[3] = dttl;
            Isian[4] = dalamat;
            Isian[5] = dorganisasi;
            Isian[6] = EM_NewPass.getTeksPass();
        }

        return  Isian;
    }

    private void  BersihkanData()
    {
        try
        {
            EM_Nama.setTeks("");
            EM_Email.setTeks("");
            EM_TTL.setTeks("");
            EM_Alamat.setTeks("");
            EM_Lembaga.setTeks("");
        }
        catch (Exception e)
        {

        }
        try
        {
            EM_OldPass.setTeksPass("");
            EM_NewPass.setTeksPass("");
            EM_Confirm.setTeksPass("");
        }
        catch (Exception e)
        {
        }
    }

    private void MasukkanData()
    {
        try
        {
            EM_Nama.setTeks(dnama);
            EM_Email.setTeks(demail);
            EM_TTL.setTeks(dttl);
            EM_Alamat.setTeks(dalamat);
            EM_Lembaga.setTeks(dorganisasi);
        }
        catch (Exception e)
        {

        }
    }

    private boolean Validasi()
    {
        boolean hasil = true;

        if (halaman_nav == 1)
        {
            if (TextUtils.isEmpty(EM_Nama.getTeks()))
            {
                Toast.makeText(getApplicationContext(), "Nama tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
            else if (TextUtils.isEmpty(EM_Email.getTeks()))
            {
                Toast.makeText(getApplicationContext(), "Email tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
            else if (TextUtils.isEmpty(EM_TTL.getTeks()))
            {
                Toast.makeText(getApplicationContext(), "Tempat, tanggal lahir tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
            else if (TextUtils.isEmpty(EM_Alamat.getTeks()))
            {
                Toast.makeText(getApplicationContext(), "Alamat tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
            else if (TextUtils.isEmpty(EM_Lembaga.getTeks()))
            {
                Toast.makeText(getApplicationContext(), "Lembaga tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
        }
        else
        {
            if (TextUtils.isEmpty(EM_OldPass.getTeksPass()))
            {
                Toast.makeText(getApplicationContext(), "Password lama tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
            else if (TextUtils.isEmpty(EM_NewPass.getTeksPass()))
            {
                Toast.makeText(getApplicationContext(), "Password baru tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
            else if (TextUtils.isEmpty(EM_Confirm.getTeksPass()))
            {
                Toast.makeText(getApplicationContext(), "Konfirmasi password tidak boleh kosong", Toast.LENGTH_LONG).show();
                hasil = false;
            }
            else if (!EM_OldPass.getTeksPass().equals(dpassword))
            {
                Toast.makeText(getApplicationContext(), "Password lama tidak cocok", Toast.LENGTH_LONG).show();
                return false;
            }
            else if (!EM_NewPass.getTeksPass().equals(EM_Confirm.getTeksPass()))
            {
                Toast.makeText(getApplicationContext(), "Password baru tidak cocok", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return hasil;
    }

    private EditTextDrawerItem BuatEditText(String nama, int icon)
    {
        return new EditTextDrawerItem().withLeftIcon(icon).withHint(nama);
    }

    private EditTextPassDrawerItem BuatEditTextPass(String nama, int icon)
    {
        return new EditTextPassDrawerItem().withLeftIcon(icon).withHint(nama);
    }

    private PrimaryDrawerItem BuatTombol(int id, String nama, int icon)
    {
        return new PrimaryDrawerItem().withName(nama)
                .withIdentifier(id)
                .withIcon(icon)
                .withSelectedColorRes(R.color.colorDrawerItemSelected)
                .withTextColorRes(R.color.colorDrawerTextItem)
                .withSelectedTextColorRes(R.color.colorDrawerTextItem)
                .withSetSelected(false);
    }

    private PrimaryDrawerItem BuatTombol(int id, String nama)
    {
        return new PrimaryDrawerItem().withName(nama)
                .withIdentifier(id)
                .withSelectedColorRes(R.color.colorDrawerItemSelected)
                .withTextColorRes(R.color.colorDrawerTextItem)
                .withSelectedTextColorRes(R.color.colorDrawerTextItem)
                .withSetSelected(false);
    }

    private void AturDrawer(IDrawerItem... items)
    {
        HeaderLayout = main_drawer.getHeader();

        if (first)
        {
            BBack = (ImageView) HeaderLayout.findViewById(R.id.BBack);
            LLProfile = (LinearLayout) HeaderLayout.findViewById(R.id.LLProfile);
            IVSigap = (ImageView) HeaderLayout.findViewById(R.id.IVSigap);
            LNama = (TextView) HeaderLayout.findViewById(R.id.LNama);
            IVFoto = (LaporanImageView) HeaderLayout.findViewById(R.id.IVFoto);

            LNama.setText(SigapApp.getNama());
            first = false;
        }

        main_drawer.removeAllItems();
        main_drawer.addItems(items);

        switch (halaman_nav)
        {
            case 0:
                Animasi.Fade(LLProfile, IVSigap, 200);
                break;
            case 1:
                Animasi.Fade(IVSigap, LLProfile, 200);
                break;
        }

        BersihkanData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        IM_EBooks = BuatTombol(1, "e-BOOKS");
        JM_Setting = new PrimaryDrawerItem().withName("PENGATURAN").withEnabled(false);
        IM_Profile = BuatTombol(2, "Profil", R.drawable.icon_profile);
        IM_ChangePass = BuatTombol(3, "Ubah Password", R.drawable.icon_changepass);
        IM_Logout = BuatTombol(4, "Logout", R.drawable.icon_logout);
        IM_Login = BuatTombol(7, "Login", R.drawable.icon_logout);
        IM_Save = BuatTombol(5, "Simpan", R.drawable.icon_save);
        IM_BangunDesa = BuatTombol(6, "Tahapan 7D");

        EM_Nama = BuatEditText("Nama", R.drawable.icon_profile);
        EM_Email = BuatEditText("Email", R.drawable.icon_email);
        EM_TTL = BuatEditText("Tempat, tanggal Lahir", R.drawable.icon_ttl);
        EM_Alamat = BuatEditText("Alamat", R.drawable.icon_alamat);
        EM_Lembaga = BuatEditText("Lembaga / Organisasi", R.drawable.icon_lembaga);
        EM_OldPass = BuatEditTextPass("Password lama", R.drawable.icon_changepass);
        EM_NewPass = BuatEditTextPass("Password baru", R.drawable.icon_changepass);
        EM_Confirm = BuatEditTextPass("Konfirmasi password", R.drawable.icon_changepass);

        BPlayEbooks = (ImageButton) findViewById(R.id.BPlayEbooks);

        halaman_nav = 0;
        main_drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withSliderBackgroundColorRes(R.color.colorCanvas)
                .withActivity(this)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withHeader(R.layout.header_layout)
                .withHeaderDivider(false)
                .withCloseOnClick(true)
                .build();

        if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
        {
            AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
        }
        else
        {
            AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
        }

        gestureDetectorCompat = new GestureDetectorCompat(this, new MyGestureListener(main_drawer));

        BBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (halaman_nav > 0)
                {
                    halaman_nav = 0;
                    if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                    {
                        AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
                    }
                    else
                    {
                        AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
                    }
                }
                else
                {
                    main_drawer.closeDrawer();
                }
            }
        });

        IVFoto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), GantiFotoActivity.class);
                intent.putExtra("url", dfoto);
                startActivity(intent);
            }
        });

        main_drawer.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener()
        {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem)
            {
                switch ((int) drawerItem.getIdentifier())
                {
                    case 1:
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                final AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                                builder2.setMessage("Versi E-Books apa yang ingin Anda pilih ?")
                                        .setCancelable(true)
                                        .setPositiveButton("Dengan Animasi", new DialogInterface.OnClickListener() {
                                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                            {
                                                Intent intent = new Intent(getApplicationContext(), EBooksActivity.class);
                                                startActivity(intent);
                                            }
                                        })
                                        .setNegativeButton("Tanpa Animasi", new DialogInterface.OnClickListener() {
                                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                                Intent intent = new Intent(getApplicationContext(), Ebooks_NoAnim.class);
                                                startActivity(intent);
                                            }
                                        });
                                final AlertDialog alert2 = builder2.create();
                                alert2.show();
                            }
                        }, 500);
                        break;
                    case 2:
                        if (SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                        {
                            Toast.makeText(getApplicationContext(), "Anda belum login", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            halaman_nav = 1;

                            EM_Nama = BuatEditText("Nama", R.drawable.icon_profile);
                            EM_Email = BuatEditText("Email", R.drawable.icon_email);
                            EM_TTL = BuatEditText("Tempat, tanggal Lahir", R.drawable.icon_ttl);
                            EM_Alamat = BuatEditText("Alamat", R.drawable.icon_alamat);
                            EM_Lembaga = BuatEditText("Lembaga / Organisasi", R.drawable.icon_lembaga);
                            EM_OldPass = BuatEditTextPass("Password lama", R.drawable.icon_changepass);
                            EM_NewPass = BuatEditTextPass("Password baru", R.drawable.icon_changepass);
                            EM_Confirm = BuatEditTextPass("Konfirmasi password", R.drawable.icon_changepass);

                            AturDrawer(EM_Nama, EM_Email, EM_TTL, EM_Alamat, EM_Lembaga, new DividerDrawerItem(), IM_Save);
                            new AmbilProfilManager(getApplicationContext()).execute(SigapApp.getPgid());
                        }
                        return true;
                    case 3:
                        if (SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                        {
                            Toast.makeText(getApplicationContext(), "Anda belum login", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            halaman_nav = 2;

                            EM_Nama = BuatEditText("Nama", R.drawable.icon_profile);
                            EM_Email = BuatEditText("Email", R.drawable.icon_email);
                            EM_TTL = BuatEditText("Tempat, tanggal Lahir", R.drawable.icon_ttl);
                            EM_Alamat = BuatEditText("Alamat", R.drawable.icon_alamat);
                            EM_Lembaga = BuatEditText("Lembaga / Organisasi", R.drawable.icon_lembaga);
                            EM_OldPass = BuatEditTextPass("Password lama", R.drawable.icon_changepass);
                            EM_NewPass = BuatEditTextPass("Password baru", R.drawable.icon_changepass);
                            EM_Confirm = BuatEditTextPass("Konfirmasi password", R.drawable.icon_changepass);

                            AturDrawer(EM_OldPass, EM_NewPass, EM_Confirm, new DividerDrawerItem(), IM_Save);
                            new AmbilProfilManager(getApplicationContext()).execute(SigapApp.getPgid());
                        }
                        return true;
                    case 4:
                        if (SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                        {
                            Toast.makeText(getApplicationContext(), "Anda belum login", Toast.LENGTH_LONG).show();
                            return true;
                        }
                        else
                        {
                            final AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                            builder2.setMessage("Apa Anda yakin ingin ganti akun ?")
                                    .setCancelable(true)
                                    .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                        {
                                            String tidakada = "Tidak Ada";

                                            SigapApp.setPgid(tidakada);
                                            SigapApp.setNama(tidakada);
                                            SigapApp.setPassword(tidakada);
                                            SigapApp.setUsername(tidakada);
                                            SigapApp.setDesa(false);
                                            SigapApp.setCofas(false);

                                            SigapApp.SimpanPengaturan();

                                            if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                                            {
                                                AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
                                            }
                                            else
                                            {
                                                AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
                                            }
                                        }
                                    })
                                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                            dialog.cancel();
                                        }
                                    });
                            final AlertDialog alert2 = builder2.create();
                            alert2.show();
                            return false;
                        }
                    case 5:
                        if (Validasi())
                        {
                            String[] isi;
                            if (halaman_nav == 1)
                            {
                                isi = AmbilIsian(1);
                            }
                            else
                            {
                                isi = AmbilIsian(0);
                            }
                            new UbahPengguna(getApplicationContext()).execute(isi[0],
                                    isi[1],
                                    isi[2],
                                    isi[3],
                                    isi[4],
                                    isi[5],
                                    isi[6]);
                            return true;
                        }
                        return true;
                    case 6:
                        if (SigapApp.getTahap1().equalsIgnoreCase("1"))
                        {
                            Intent intent;
                            if (SigapApp.getTahap2().equalsIgnoreCase("1"))
                            {
                                if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada") && SigapApp.isCofas() && SigapApp.isDesa())
                                {
                                    intent = new Intent(getApplicationContext(), TahapActivity.class);
                                }
                                else if ((SigapApp.isCofas() == false || SigapApp.isDesa() == false) && !SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                                {
                                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                                    intent.putExtra("fungsi", "LENGKAPI");
                                }
                                else
                                {
                                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                                }
                            }
                            else
                            {
                                intent = new Intent(getApplicationContext(), PlaySigapActivity.class);
                            }
                            startActivity(intent);
                            return false;
                        }
                        else
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setMessage("Kami rekomendasikan Anda untuk membaca E-Books dan menyelesaikan Quiz terlebih dahulu untuk dapat melaksanakan ini.\n\nApakah Anda ingin melihat E-Books ?")
                                    .setCancelable(true)
                                    .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                        {
                                            Intent intent = new Intent(getApplicationContext(), EBooksActivity.class);
                                            startActivity(intent);
                                        }
                                    })
                                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                            dialog.cancel();
                                        }
                                    });
                            final AlertDialog alert = builder.create();
                            alert.show();
                            return false;
                        }
                    case 7:
                        if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                        {
                            Toast.makeText(getApplicationContext(), "Anda sudah login", Toast.LENGTH_LONG).show();
                            return true;
                        }
                        else
                        {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.putExtra("fungsi", "LOGIN");
                            startActivity(intent);
                            return false;
                        }
                }

                return false;
            }
        });

        BPlayEbooks.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                builder2.setMessage("Versi E-Books apa yang ingin Anda pilih ?")
                        .setCancelable(true)
                        .setPositiveButton("Dengan Animasi", new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                            {
                                Intent intent = new Intent(getApplicationContext(), EBooksActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Tanpa Animasi", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                Intent intent = new Intent(getApplicationContext(), Ebooks_NoAnim.class);
                                startActivity(intent);
                            }
                        });
                final AlertDialog alert2 = builder2.create();
                alert2.show();
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener
    {
        Drawer drawer;

        public MyGestureListener(Drawer drawer)
        {
            this.drawer = drawer;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
        {
            if((event1.getX() < event2.getX()) && velocityX > 100)
            {
                drawer.openDrawer();
            }
            return true;
        }
    }

    class AmbilProfilManager extends AsyncTask<String, Void, String>
    {
        ProgressDialog pDialog;
        private Context context;

        public AmbilProfilManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Memuat...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            RequestHandler rh = new RequestHandler();
            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);

                link = getString(R.string.alamat_server) + getString(R.string.link_ambil_profil_pengguna);
                result = rh.sendPostRequest(link, data);;

                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = Crypt.Decrypt(result);
            Log.i("### # # # ##", "onPostExecute: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String hasil = jsonObj.getString("hasil");
                    if (hasil.equals("SUKSES")) {
                        if (imageLoader == null)
                            imageLoader = AppController.getInstance().getImageLoader();

                        dnama = jsonObj.getString("nama");
                        demail = jsonObj.getString("email");
                        dttl = jsonObj.getString("ttl");
                        dalamat = jsonObj.getString("alamat");
                        dorganisasi = jsonObj.getString("organisasi");
                        dpassword = jsonObj.getString("password");
                        dfoto = jsonObj.getString("foto");

                        if (dfoto != null) {
                            IVFoto.setImageUrl(dfoto, imageLoader);
                            IVFoto.setVisibility(View.VISIBLE);
                            IVFoto.setResponseObserver(new LaporanImageView.ResponseObserver() {
                                @Override
                                public void onError() {
                                }

                                @Override
                                public void onSuccess() {
                                }
                            });
                        } else {
                            IVFoto.setImageResource(R.drawable.gambar_avatar);
                        }

                        IVFoto.setErrorImageResId(R.drawable.gambar_avatar);

                        MasukkanData();
                    } else {
                        halaman_nav = 0;
                        if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                        {
                            AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
                        }
                        else
                        {
                            AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
                        }
                        Snackbar.make(LLProfile, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                } catch (JSONException e)
                {
                    halaman_nav = 0;
                    if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                    {
                        AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
                    }
                    else
                    {
                        AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
                    }
                    Snackbar.make(LLProfile, "Ditemukan error. " + e.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            } else
            {
                halaman_nav = 0;
                if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                {
                    AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
                }
                else
                {
                    AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
                }
                Snackbar.make(LLProfile, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    class UbahPengguna extends AsyncTask<String, Void, String>
    {
        ProgressDialog pDialog;
        private Context context;
        private String mpassword, musername, mnama;

        public UbahPengguna(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Mengubah...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);
                data.put("nama", arg0[1]);
                data.put("email", arg0[2]);
                data.put("ttl", arg0[3]);
                data.put("alamat", arg0[4]);
                data.put("organisasi", arg0[5]);
                data.put("password", arg0[6]);

                mpassword = arg0[6];
                musername = arg0[2];
                mnama = arg0[1];

                link = getString(R.string.alamat_server) + getString(R.string.link_ubah_pengguna);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            Log.i("RESULT", "onPostExecute: " + result);
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        halaman_nav = 0;
                        if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                        {
                            AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
                        }
                        else
                        {
                            AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
                        }
                        Toast.makeText(context, "Perubahan sudah disimpan", Toast.LENGTH_SHORT).show();

                        SigapApp.setNama(mnama);
                        SigapApp.setUsername(musername);
                        SigapApp.setPassword(mpassword);
                        SigapApp.SimpanPengaturan();

                        LNama.setText(SigapApp.getNama());
                    } else if (query_result.equals("GAGAL"))
                    {
                        Toast.makeText(context, "Pendaftaran gagal.", Toast.LENGTH_SHORT).show();
                    } else if (query_result.equals("EMAIL TIDAK VALID"))
                    {
                        Toast.makeText(context, "Email sudah dipakai.", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(context, "Tidak dapat terhubung ke database.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Toast.makeText(context, "Tidak dapat mengambil data JSON.", Toast.LENGTH_SHORT).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    @Override
    protected void onResume()
    {
        if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
        {
            AturDrawer(IM_EBooks, IM_BangunDesa, new DividerDrawerItem(), JM_Setting, IM_Profile, IM_ChangePass, new DividerDrawerItem(), IM_Logout);
        }
        else
        {
            AturDrawer(IM_EBooks, new DividerDrawerItem(), IM_Login);
        }
        super.onResume();
    }
}
