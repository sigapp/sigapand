package com.sigapp.id;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.sigapp.id.App.SigapApp;

public class HasilQuizActivity extends AppCompatActivity
{
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasilquiz);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        ImageView IVPoster = (ImageView) findViewById(R.id.IVPoster);
        ImageView BLanjut = (ImageView) findViewById(R.id.BLanjut);

        SigapApp.setTahap1("1");
        SigapApp.SimpanPengaturan();

        IVPoster.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), FotoActivity.class);
                intent.putExtra("judul", "Poster SIGAP REDD+");
                intent.putExtra("drawable", R.drawable.gambar_poster);
                startActivity(intent);
            }
        });

        BLanjut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
                Intent intent = new Intent(getApplicationContext(), PilihanActivity.class);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        Boolean benarsemua = intent.getBooleanExtra("benarsemua", false);
    }
}
