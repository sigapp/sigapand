package com.sigapp.id;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.sigapp.id.App.SigapApp;

public class PilihanActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilihan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        ImageView IVPendamping = (ImageView) findViewById(R.id.IVPendamping);
        ImageView IVWarga = (ImageView) findViewById(R.id.IVWarga);
        ImageView BPendamping = (ImageView) findViewById(R.id.BPendamping);
        ImageView BWarga = (ImageView) findViewById(R.id.BWarga);

        BWarga.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String appPackageName = "org.wordpress.android";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        IVWarga.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String appPackageName = "org.wordpress.android";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        BPendamping.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent;
                if (SigapApp.getTahap2().equalsIgnoreCase("1"))
                {
                    if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada") && SigapApp.isCofas() && SigapApp.isDesa())
                    {
                        intent = new Intent(getApplicationContext(), TahapActivity.class);
                    }
                    else if ((SigapApp.isCofas() == false || SigapApp.isDesa() == false) && !SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                    {
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.putExtra("fungsi", "LENGKAPI");
                    }
                    else
                    {
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                    }
                }
                else
                {
                    intent = new Intent(getApplicationContext(), PlaySigapActivity.class);
                }
                startActivity(intent);
            }
        });

        IVPendamping.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent;
                if (SigapApp.getTahap2().equalsIgnoreCase("1"))
                {
                    if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada") && SigapApp.isCofas() && SigapApp.isDesa())
                    {
                        intent = new Intent(getApplicationContext(), TahapActivity.class);
                    }
                    else if ((SigapApp.isCofas() == false || SigapApp.isDesa() == false) && !SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                    {
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.putExtra("fungsi", "LENGKAPI");
                    }
                    else
                    {
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                    }
                }
                else
                {
                    intent = new Intent(getApplicationContext(), PlaySigapActivity.class);
                }
                startActivity(intent);
            }
        });
    }
}
