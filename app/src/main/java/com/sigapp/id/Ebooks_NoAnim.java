package com.sigapp.id;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.sigapp.id.Adapter.ImageItemAdapter;
import com.sigapp.id.App.SigapApp;
import com.sigapp.id.Data.ImageItem;

import java.util.ArrayList;
import java.util.List;

public class Ebooks_NoAnim extends AppCompatActivity
{
    private GridView GVTahap;
    private ImageItemAdapter listAdapter;
    private List<ImageItem> items;

    private int[] gambar = { R.drawable.icon_pengantar,
            R.drawable.icon_tahap1,
            R.drawable.icon_tahap2,
            R.drawable.icon_tahap3,
            R.drawable.icon_tahap4,
            R.drawable.icon_tahap5,
            R.drawable.icon_tahap6,
            R.drawable.icon_tahap7,
            R.drawable.icon_penutup,
            R.drawable.icon_video};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebooksnoanim);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        GVTahap = (GridView) findViewById(R.id.GVTahap);

        items = new ArrayList<ImageItem>();
        listAdapter = new ImageItemAdapter(this, items);
        GVTahap.setAdapter(listAdapter);

        int a = 0;

        for (int i : gambar)
        {
            ImageItem item = new ImageItem();
            item.setDrawable(i);
            item.setId(a);
            items.add(item);
            a++;
        }

        GVTahap.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (position == 9)
                {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(Ebooks_NoAnim.this);
                    builder.setMessage("Silakan pilih bahasa dari video SIGAP REDD+ yang ingin Anda tonton.")
                            .setCancelable(true)
                            .setPositiveButton("Bahasa Indonesia", new DialogInterface.OnClickListener()
                            {
                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                                {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=so5ybv8fRKs&feature=youtu.be")));
                                }
                            })
                            .setNegativeButton("Language English", new DialogInterface.OnClickListener()
                            {
                                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=1dCE30j0hQU&feature=youtu.be")));
                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();
                }
                else
                {
                    Intent intent = new Intent(getApplicationContext(), IsiEBooksActivity.class);
                    intent.putExtra("halaman", position);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
                }
            }
        });

        GVTahap.deferNotifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ebooks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_skip)
        {
            finish();

            if (SigapApp.getCurkuis().equalsIgnoreCase("20"))
            {
                Intent intent = new Intent(getApplicationContext(), PilihanActivity.class);
                startActivity(intent);
            }
            else
            {
                Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
