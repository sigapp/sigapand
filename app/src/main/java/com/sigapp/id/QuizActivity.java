package com.sigapp.id;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.sigapp.id.App.Animasi;
import com.sigapp.id.App.SigapApp;

public class QuizActivity extends AppCompatActivity
{
    private static ImageView BBenar, BSalah, BMulai, BKirimJawaban;
    private static LinearLayout LLTombol, LLPertanyaan;
    private static ScrollView SVAwal;
    private static TextView LKeterangan, LPertanyaan, LStatusPertanyaan, LTotalNilai;
    private static int soalke, nilai;

    private static String JawabanPengguna;

    private Toolbar toolbar;

    static String[] soal = {"Pendekatan 7D hanya bisa digunakan khusus untuk program terkait REDD+",
            "Dalam pendekatan 7D, kelemahan-kelemahan masyarakat perlu diidentifikasi",
            "Tahap D1 (disclosure) merupakan tahap dimana fasilitator dan masyarakat berusaha saling mengenal",
            "Output utama tahap D1 adalah tersusunnya laporan dan terbangunnya kepercayaan masyarakat, dengan indikator utama fasilitator terlibat aktif dan dilibatkan dalam berbagai aktivitas warga",
            "Tahap D2 (define) merupakan tahap dimana fasilitator menentukan tema",
            "Pada akhir tahap D2, warga telah memilih tiga tema percakapan utama berkenaan dengan kemajuan dan keberlanjutan kampung/desa, dengan indikator utama tiga topik percakapan setahun ke depan",
            "Tahap D3 (discovery) merupakan tahap dimana fasilitator membantu masyarakat mengenali aset-aset yang dimiliki masyarakat",
            "Yang dimaksud aset-aset yang dimiliki masyarakat adalah uang tunai yang dimiliki masyarakat sebagai biaya untuk membangun",
            "Output dan indikator utama tahap D3 adalah wawancara apresiatif seluruh warga dan peta kekuatan kampung",
            "Pada tahap D4 (dream) masyarakat harus fokus pada kebutuhan, bukan keinginan",
            "Output tahap D4 adalah pertemuan impian masyarakat, mural impian masyarakat, dan peta tiga dimensi kampung dalam ukuran besar",
            "Tahap D5 (design) adalah tahap dimana aksi-aksi inspiratif untuk mewujudkan mimpi dirancang",
            "Pada tahap D5, fasilitator memperkenalkan model tata guna lahan tiga dimensi yang menggambarkan wilayah kampung dan bentukan bentang alam sekitar",
            "Output tahap D5 adalah Dokumen Perencanaan Kampung dan Strategi Penganggaran",
            "Tahap D6 (delivery) adalah tahap dimana aksi-aksi inspiratif mulai dilaksanakan",
            "Output tahap D6 terdiri dari foto dokumentasi kegiatan, foto dokumentasi pertemuan pelaksanaan program, dan Laporan Kemajuan",
            "Pada tahap D7 (drive) masyarakat merayakan capaian mimpi-mimpi yang telah dibangun",
            "Output tahap D7 adalah foto dokumentasi proses dan pelaksanaan Perayaan Kampung",
            "Salah satu tujuan perayaan dalam tahap pelibatan masyarakat adalah untuk menginspirasi dan menggerakkan masyarakat guna melakukan perubahan-perubahan positif lain",
            "Pendekatan 7D sangat cocok untuk merangsang kemandirian masyarakat"};

    static int[] jawaban = {0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    static int jawabanuser = 2;

    private static void GantiSoal()
    {
        jawabanuser = 2;
        BKirimJawaban.setEnabled(true);
        BBenar.setEnabled(true);
        BSalah.setEnabled(true);
        BBenar.setColorFilter(Color.argb(200, 200, 200, 200));
        BSalah.setColorFilter(Color.argb(200, 200, 200, 200));
        Animasi.Fade(LStatusPertanyaan, false, 200, false);

        Animasi.Fade(LLPertanyaan, LLPertanyaan, 200);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {
                try
                {
                    LKeterangan.setText(String.valueOf(soalke + 1) + " / " + String.valueOf(soal.length));
                    LPertanyaan.setText(soal[soalke]);
                }
                catch (Exception e)
                {

                }
            }
        }, 250);
    }

    public static void Blink(final View view)
    {
        view.setAlpha(0.5f);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {
                view.setAlpha(1f);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        view.setAlpha(0.5f);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                view.setAlpha(1f);
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        view.setAlpha(0.5f);
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run()
                                            {
                                                view.setAlpha(1f);
                                                final Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run()
                                                    {
                                                        Next();
                                                    }
                                                }, 500);
                                            }
                                        }, 250);
                                    }
                                }, 250);
                            }
                        }, 250);
                    }
                }, 250);
            }
        }, 250);
    }

    private static boolean JawabPertanyaan(int pilihan)
    {
        JawabanPengguna += String.valueOf(pilihan);
        SigapApp.setJawaban(JawabanPengguna);
        SigapApp.SimpanPengaturan();
        if (jawaban[soalke] == pilihan)
        {
            nilai = nilai + 10;
            Animasi.Fade(LTotalNilai, 200, String.valueOf(nilai));
            SigapApp.setNilaiquiz(nilai);
            SigapApp.SimpanPengaturan();
        }
        else
        {
            String a;
            if (jawaban[soalke] == 0)
            {
                LStatusPertanyaan.setText("Jawaban yang benar adalah SALAH");
                Animasi.Fade(LStatusPertanyaan, LStatusPertanyaan, 200, false);
            }
            else
            {
                LStatusPertanyaan.setText("Jawaban yang benar adalah BENAR");
                Animasi.Fade(LStatusPertanyaan, LStatusPertanyaan, 200, false);
            }

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    Next();
                }
            }, 2000);

            return false;
        }

        soalke++;
        SigapApp.setCurkuis(String.valueOf(soalke));
        SigapApp.SimpanPengaturan();
        if (soalke < soal.length)
        {
            GantiSoal();
        }
        else
        {
            activity.finish();
            Intent intent = new Intent(cc, HasilQuizActivity.class);
            activity.startActivity(intent);
        }

        return true;
    }

    private static void Next()
    {
        soalke++;
        if (soalke < soal.length)
        {
            GantiSoal();
        }
        else
        {
            activity.finish();
            Intent intent = new Intent(cc, HasilQuizActivity.class);
            activity.startActivity(intent);
        }
    }

    static Context cc;
    static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        cc = getApplicationContext();
        activity = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        SVAwal = (ScrollView) findViewById(R.id.SVAwal);
        LLPertanyaan = (LinearLayout) findViewById(R.id.LLPertanyaan);
        LLTombol = (LinearLayout) findViewById(R.id.LLTombol);
        BMulai = (ImageView) findViewById(R.id.BMulai);
        BBenar = (ImageView) findViewById(R.id.BBenar);
        BSalah = (ImageView) findViewById(R.id.BSalah);
        BKirimJawaban = (ImageView) findViewById(R.id.BKirimJawaban);
        LKeterangan = (TextView) findViewById(R.id.LKeterangan);
        LPertanyaan = (TextView) findViewById(R.id.LPertanyaan);
        LStatusPertanyaan = (TextView) findViewById(R.id.LStatusPertanyaan);
        LTotalNilai = (TextView) findViewById(R.id.LTotalNilai);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SigapApp.getTinggi_layar() / 3);
        layoutParams.setMargins(0, 10, 0, 0);

        LLPertanyaan.setLayoutParams(layoutParams);

        LinearLayout LLLTombol = (LinearLayout) findViewById(R.id.LLLTombol);
        ImageView IVIlustrasi = (ImageView) findViewById(R.id.IVIlustrasi);

        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SigapApp.getTinggi_layar() / 7);
        layoutParams2.setMargins(0, 0, 0, 0);

        LLLTombol.setLayoutParams(layoutParams2);

        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SigapApp.getTinggi_layar() / 4);
        layoutParams3.setMargins(0, 10, 0, 0);

        IVIlustrasi.setLayoutParams(layoutParams3);

        BMulai.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Animasi.Fade(SVAwal, LLTombol, 200);
                soalke = Integer.valueOf(SigapApp.getCurkuis());
                nilai = SigapApp.getNilaiquiz();
                LTotalNilai.setText(String.valueOf(nilai));
                GantiSoal();
            }
        });

        BBenar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                BBenar.clearColorFilter();
                BSalah.setColorFilter(Color.argb(200, 200, 200, 200));
                jawabanuser = 1;
            }
        });

        BSalah.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                BSalah.clearColorFilter();
                BBenar.setColorFilter(Color.argb(200, 200, 200, 200));
                jawabanuser = 0;
            }
        });

        BKirimJawaban.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (jawabanuser != 2)
                {
                    BKirimJawaban.setEnabled(false);
                    BBenar.setEnabled(false);
                    BSalah.setEnabled(false);
                    JawabPertanyaan(jawabanuser);
                }
                else
                {
                    Snackbar.make(BKirimJawaban, "Pilih jawaban terlebih dahulu", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }
}
