package com.sigapp.id.App;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 10 Agustus 2016.
 */
public class Crypt
{
    public static final String PASSWORD_CRYPT = "sigapulalaoyeahtratakdungces";

    private static int Konversi(char karakter)
    {
        return (int) karakter;
    }

    private static char Deversi(int nilai)
    {
        return (char) nilai;
    }

    public static String Encrypt(String teks)
    {
        String kunci = PASSWORD_CRYPT;
        String h = "";
        char[] cc = kunci.toCharArray();
        int b = 0;
        int i;

        for (char c : teks.toCharArray())
        {
            i = Konversi(c) + Konversi(cc[b]);
            b++;
            if (b == kunci.length())
            {
                b = 0;
            }

            h = h + Deversi(i);
        }

        return h;
    }

    public static String Decrypt(String teks)
    {
        String kunci = PASSWORD_CRYPT;
        String h = "";
        char[] cc = kunci.toCharArray();
        int b = 0;
        int i;

        for (char c : teks.toCharArray())
        {
            i = Konversi(c) - Konversi(cc[b]);
            b++;
            if (b == kunci.length())
            {
                b = 0;
            }

            h = h + Deversi(i);
        }

        return h;
    }
}
