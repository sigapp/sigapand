package com.sigapp.id.App;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.sigapp.id.R;

import java.io.File;

/**
 * Created by Aziz Nur Ariffianto on 18 Agustus 2016.
 */
public class SigapApp
{
    private static Context cc;
    private static int lebar_layar, tinggi_layar, dpi, nilaiquiz;
    private static float density;
    private static SharedPreferences sharedPref;
    private static String jawaban, password, tahap1, username, pgid, nama, tahap2, timecurlaporan, curkuis;
    private static float offsetukuranhuruf;
    private static boolean cofas, desa;
    public static final String KARAKTER_URI = "@#&=*+-_.,:!?()/~'%";

    private static int[] judul = { R.drawable.gambar_tahap1,
            R.drawable.gambar_tahap2,
            R.drawable.gambar_tahap3,
            R.drawable.gambar_tahap4,
            R.drawable.gambar_tahap5,
            R.drawable.gambar_tahap6,
            R.drawable.gambar_tahap7};

    public static int getLebar_layar()
    {
        return lebar_layar;
    }

    public static int getTinggi_layar()
    {
        return tinggi_layar;
    }

    public static Context getCc()
    {
        return cc;
    }

    public static void setJawaban(String jawaban)
    {
        SigapApp.jawaban = jawaban;
    }

    public static void setPassword(String password)
    {
        SigapApp.password = password;
    }

    public static void setTahap1(String tahap1)
    {
        SigapApp.tahap1 = tahap1;
    }

    public static void setUsername(String username)
    {
        SigapApp.username = username;
    }

    public static void setPgid(String pgid)
    {
        SigapApp.pgid = pgid;
    }

    public static String getJawaban()
    {
        return jawaban;
    }

    public static String getPassword()
    {
        return password;
    }

    public static String getTahap1()
    {
        return tahap1;
    }

    public static String getUsername()
    {
        return username;
    }

    public static String getPgid()
    {
        return pgid;
    }

    public static String getNama()
    {
        return nama;
    }

    public static void setNama(String nama)
    {
        SigapApp.nama = nama;
    }

    public static int getDpi()
    {
        return dpi;
    }

    public static String getTahap2()
    {
        return tahap2;
    }

    public static void setTahap2(String tahap2)
    {
        SigapApp.tahap2 = tahap2;
    }

    public static String getTimecurlaporan()
    {
        return timecurlaporan;
    }

    public static void setTimecurlaporan(String timecurlaporan)
    {
        SigapApp.timecurlaporan = timecurlaporan;
    }

    public static int getNilaiquiz()
    {
        return nilaiquiz;
    }

    public static void setNilaiquiz(int nilaiquiz)
    {
        SigapApp.nilaiquiz = nilaiquiz;
    }

    public static int[] getJudul()
    {
        return judul;
    }

    public static float getOffsetukuranhuruf()
    {
        return offsetukuranhuruf;
    }

    public static void setOffsetukuranhuruf(float offsetukuranhuruf)
    {
        SigapApp.offsetukuranhuruf = offsetukuranhuruf;
    }

    public static String getCurkuis()
    {
        return curkuis;
    }

    public static void setCurkuis(String curkuis)
    {
        SigapApp.curkuis = curkuis;
    }

    public static float getDensity()
    {
        return density;
    }

    public static boolean isCofas()
    {
        return cofas;
    }

    public static void setCofas(boolean cofas)
    {
        SigapApp.cofas = cofas;
    }

    public static boolean isDesa()
    {
        return desa;
    }

    public static void setDesa(boolean desa)
    {
        SigapApp.desa = desa;
    }

    public SigapApp(Context cc)
    {
        this.cc = cc;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) cc.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

        lebar_layar = displayMetrics.widthPixels;
        tinggi_layar = displayMetrics.heightPixels;

        dpi = displayMetrics.densityDpi;
        density = displayMetrics.density;

        sharedPref = cc.getSharedPreferences("com.sigapp.id", Context.MODE_PRIVATE);
        BacaPengaturan();
    }

    public static void BacaPengaturan()
    {
        password = Crypt.Decrypt(sharedPref.getString(cc.getString(R.string.id_save_password), Crypt.Encrypt("Tidak Ada")));
        username = Crypt.Decrypt(sharedPref.getString(cc.getString(R.string.id_save_username), Crypt.Encrypt("Tidak Ada")));
        nama = Crypt.Decrypt(sharedPref.getString(cc.getString(R.string.id_save_nama), Crypt.Encrypt("Tidak Ada")));
        timecurlaporan = Crypt.Decrypt(sharedPref.getString(cc.getString(R.string.id_save_timecurlaporan), Crypt.Encrypt("Tidak Ada")));

        jawaban = sharedPref.getString(cc.getString(R.string.id_save_jawaban), "Tidak Ada");
        tahap1 = sharedPref.getString(cc.getString(R.string.id_save_tahap1), "Tidak Ada");
        pgid = sharedPref.getString(cc.getString(R.string.id_save_pgid), "Tidak Ada");
        tahap2 = sharedPref.getString(cc.getString(R.string.id_save_tahap2), "Tidak Ada");
        curkuis = sharedPref.getString(cc.getString(R.string.id_save_curkuis), "0");
        offsetukuranhuruf = sharedPref.getFloat(cc.getString(R.string.id_save_offsetukuranhuruf), 0f);
        nilaiquiz = sharedPref.getInt(cc.getString(R.string.id_save_nilaiquiz), 0);
        cofas = sharedPref.getBoolean(cc.getString(R.string.id_save_cofas), false);
        desa = sharedPref.getBoolean(cc.getString(R.string.id_save_desa), false);
    }

    public static void SimpanPengaturan()
    {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(cc.getString(R.string.id_save_password), Crypt.Encrypt(password));
        editor.putString(cc.getString(R.string.id_save_username), Crypt.Encrypt(username));
        editor.putString(cc.getString(R.string.id_save_nama), Crypt.Encrypt(nama));
        editor.putString(cc.getString(R.string.id_save_timecurlaporan), Crypt.Encrypt(timecurlaporan));

        editor.putString(cc.getString(R.string.id_save_jawaban), jawaban);
        editor.putString(cc.getString(R.string.id_save_tahap1), tahap1);
        editor.putString(cc.getString(R.string.id_save_pgid), pgid);
        editor.putString(cc.getString(R.string.id_save_tahap2), tahap2);
        editor.putString(cc.getString(R.string.id_save_curkuis), curkuis);
        editor.putFloat(cc.getString(R.string.id_save_offsetukuranhuruf), offsetukuranhuruf);
        editor.putInt(cc.getString(R.string.id_save_nilaiquiz), nilaiquiz);
        editor.putBoolean(cc.getString(R.string.id_save_cofas), cofas);
        editor.putBoolean(cc.getString(R.string.id_save_desa), desa);
        editor.commit();
    }

    public static void Bagikan(Activity cc, String judul, String isi, String urlimage)
    {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, judul);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, isi + "\n" + "<img src=\"" + urlimage + "\" alt=\"alt text here\">");
        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cc.startActivity(Intent.createChooser(sharingIntent, "Bagikan lewat"));
    }
}
