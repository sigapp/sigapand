package com.sigapp.id.App;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.LayoutRes;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
import com.mikepenz.materialdrawer.model.BasePrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.BaseViewHolder;
import com.sigapp.id.R;

/**
 * Created by Aziz Nur Ariffianto on 17 Agustus 2016.
 */

public class EditTextDrawerItem extends BasePrimaryDrawerItem<EditTextDrawerItem, EditTextDrawerItem.ViewHolder>
{
    private String teks, hint;
    private int icon;
    private ViewHolder bviewHolder;

    public String getTeks()
    {
        return teks;
    }

    public void setTeks(String teks)
    {
        this.teks = teks;
        bviewHolder.editTextView.setText(teks);
    }

    public EditTextDrawerItem withHint(String hint)
    {
        this.hint = hint;
        return this;
    }

    public EditTextDrawerItem withLeftIcon(int icon)
    {
        this.icon = icon;
        return this;
    }

    @Override
    public int getType() {
        return R.id.EditText;
    }

    @Override
    @LayoutRes
    public int getLayoutRes() {
        return R.layout.edit_text;
    }

    @Override
    public void bindView(final ViewHolder viewHolder)
    {
        Context ctx = viewHolder.itemView.getContext();

        bviewHolder = viewHolder;

        viewHolder.editTextView.setHint(hint);
        viewHolder.icon.setImageResource(icon);

        viewHolder.editTextView.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                teks = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                teks = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                teks = s.toString();
            }
        });

        onPostBindView(this, viewHolder.itemView);
    }

    @Override
    public ViewHolderFactory<ViewHolder> getFactory() {
        return new ItemFactory();
    }

    public static class ItemFactory implements ViewHolderFactory<ViewHolder>
    {
        public ViewHolder create(View v)
        {
            return new ViewHolder(v);
        }
    }

    public static class ViewHolder extends BaseViewHolder
    {
        private EditText editTextView;
        private ImageView icon;

        private ViewHolder(View view) {
            super(view);
            this.icon = (ImageView) view.findViewById(R.id.IVIcon);
            this.editTextView = (EditText) view.findViewById(R.id.EditText);
        }
    }
}