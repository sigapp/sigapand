package com.sigapp.id.App;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Aziz Nur Ariffianto on 26 Agustus 2016.
 */
public class TanggalHandler
{
    public static String KonversiTanggal(String data, boolean full)
    {
        String expectedPattern, format;
        if (full)
        {
            expectedPattern = "yyyy-MM-dd HH:mm:ss";
            format = "dd MMMM yyyy hh:mm a";
        }
        else
        {
            expectedPattern = "yyyy-MM-dd";
            format = "dd MMMM yyyy";
        }
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        Date tanggal = null;
        try
        {
            tanggal = formatter.parse(data);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        sdf.setTimeZone(cal.getTimeZone());

        return sdf.format(tanggal);
    }
}
