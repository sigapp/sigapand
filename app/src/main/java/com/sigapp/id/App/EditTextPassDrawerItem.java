package com.sigapp.id.App;

import android.support.annotation.LayoutRes;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.mikepenz.materialdrawer.model.BasePrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.BaseViewHolder;
import com.sigapp.id.R;

/**
 * Created by Aziz Nur Ariffianto on 17 Agustus 2016.
 */

public class EditTextPassDrawerItem extends BasePrimaryDrawerItem<EditTextPassDrawerItem, EditTextPassDrawerItem.ViewHolder>
{
    private String teksPass, hintPass;
    private int iconPass;
    private ViewHolder bviewHolder;

    public String getTeksPass()
    {
        return teksPass;
    }

    public void setTeksPass(String teksPass)
    {
        this.teksPass = teksPass;
        bviewHolder.editTextViewPass.setText(teksPass);
    }

    public EditTextPassDrawerItem withHint(String hint)
    {
        this.hintPass = hint;
        return this;
    }

    public EditTextPassDrawerItem withLeftIcon(int icon)
    {
        this.iconPass = icon;
        return this;
    }

    @Override
    public int getType() {
        return R.id.EditTextPass;
    }

    @Override
    @LayoutRes
    public int getLayoutRes() {
        return R.layout.edit_text_pass;
    }

    @Override
    public void bindView(final ViewHolder viewHolder)
    {
        bviewHolder = viewHolder;

        viewHolder.editTextViewPass.setHint(hintPass);
        viewHolder.iconPass.setImageResource(iconPass);

        viewHolder.editTextViewPass.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                teksPass = s.toString();
            }
        });

        onPostBindView(this, viewHolder.itemView);
    }

    @Override
    public ViewHolderFactory<ViewHolder> getFactory() {
        return new ItemFactory();
    }

    public static class ItemFactory implements ViewHolderFactory<ViewHolder>
    {
        public ViewHolder create(View v)
        {
            return new ViewHolder(v);
        }
    }

    public static class ViewHolder extends BaseViewHolder
    {
        private EditText editTextViewPass;
        private ImageView iconPass;

        private ViewHolder(View view) {
            super(view);
            this.iconPass = (ImageView) view.findViewById(R.id.IVIcon);
            this.editTextViewPass = (EditText) view.findViewById(R.id.EditTextPass);
        }
    }
}