package com.sigapp.id.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sigapp.id.App.SigapApp;
import com.sigapp.id.App.TanggalHandler;
import com.sigapp.id.Data.TahapanItem;
import com.sigapp.id.LaporanTahapActivity;
import com.sigapp.id.PersiapanTahapActivity;
import com.sigapp.id.PlaySigapActivity;
import com.sigapp.id.R;
import com.sigapp.id.SelesaiActivity;

import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 26 Agustus 2016.
 */
public class TahapanItemAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<TahapanItem> tahapanItems;

    public TahapanItemAdapter(Activity activity, List<TahapanItem> tahapanItems)
    {
        this.activity = activity;
        this.tahapanItems = tahapanItems;
    }

    @Override
    public int getCount()
    {
        return tahapanItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return tahapanItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.tahapan_item, null);

        RelativeLayout RLInfo1 = (RelativeLayout) convertView.findViewById(R.id.RLInfo1);
        RelativeLayout RLInfo2 = (RelativeLayout) convertView.findViewById(R.id.RLInfo2);
        RelativeLayout RLInfo3 = (RelativeLayout) convertView.findViewById(R.id.RLInfo3);
        RelativeLayout RLInfoWaktu = (RelativeLayout) convertView.findViewById(R.id.RLInfoWaktu);
        TextView LInfo1 = (TextView) convertView.findViewById(R.id.LInfo1);
        TextView LInfo2 = (TextView) convertView.findViewById(R.id.LInfo2);
        TextView LInfo3 = (TextView) convertView.findViewById(R.id.LInfo3);
        final TextView LInfoWaktu = (TextView) convertView.findViewById(R.id.LInfoWaktu);
        TextView LInfo = (TextView) convertView.findViewById(R.id.LInfo);
        TextView LKeterangan = (TextView) convertView.findViewById(R.id.LKeterangan);
        ImageView IVJudul = (ImageView) convertView.findViewById(R.id.IVJudul);
        ImageView BMulai = (ImageView) convertView.findViewById(R.id.BMulai);
        ImageView BSelesai = (ImageView) convertView.findViewById(R.id.BSelesai);
        ImageView BLihatLaporan = (ImageView) convertView.findViewById(R.id.BLihatLaporan);
        LinearLayout LLMain = (LinearLayout) convertView.findViewById(R.id.LLMain);
        LinearLayout LLDone = (LinearLayout) convertView.findViewById(R.id.LLDone);

        final TahapanItem item = tahapanItems.get(position);

        if (item.isSelesai())
        {
            LLMain.setVisibility(View.GONE);
            LLDone.setVisibility(View.VISIBLE);
            BSelesai.setEnabled(true);
        }
        else
        {
            LLMain.setVisibility(View.VISIBLE);
            BSelesai.setEnabled(false);
            LLDone.setVisibility(View.GONE);
        }

        try
        {
            IVJudul.setImageResource(SigapApp.getJudul()[Integer.valueOf(item.getTahap()) - 1]);
        }
        catch (Exception e)
        {

        }
        if (!TextUtils.isEmpty(item.getWaktuambil()))
        {
            RLInfo1.setVisibility(View.VISIBLE);
            LInfo1.setText("Diambil pada: " + TanggalHandler.KonversiTanggal(item.getWaktuambil(), false));
        }
        else
        {
            RLInfo1.setVisibility(View.GONE);
        }

        LKeterangan.setText(item.getKeterangan());

        if (item.getStatus().equalsIgnoreCase("0"))
        {
            LInfo.setText("Belum Diambil");
        }
        else if (item.getStatus().equalsIgnoreCase("1"))
        {
            LInfo.setText("Sedang Berjalan");
        }
        else if (item.getStatus().equalsIgnoreCase("2"))
        {
            LInfo.setText("Sudah Selesai");
        }

        if (!TextUtils.isEmpty(item.getWaktu()))
        {
            if (item.getStatus().equalsIgnoreCase("0"))
            {
                RLInfoWaktu.setVisibility(View.VISIBLE);
                LInfoWaktu.setText("Batas waktu: " + item.getWaktu() + " hari");
            }
            else if (item.getStatus().equalsIgnoreCase("1"))
            {
                RLInfoWaktu.setVisibility(View.VISIBLE);
                LInfoWaktu.setText("Sisa waktu: " + item.getSisawaktu() + " hari lagi");
            }
            else if (item.getStatus().equalsIgnoreCase("2"))
            {
                RLInfoWaktu.setVisibility(View.VISIBLE);
                LInfoWaktu.setText("Lama waktu: " + item.getWaktu() + " hari");
            }
        }
        else
        {
            if (item.getStatus().equalsIgnoreCase("0"))
            {
                RLInfoWaktu.setVisibility(View.GONE);
            }
            else if (item.getStatus().equalsIgnoreCase("1"))
            {
                RLInfoWaktu.setVisibility(View.GONE);
            }
            else if (item.getStatus().equalsIgnoreCase("2"))
            {
                RLInfo2.setVisibility(View.GONE);
            }
        }

        if (item.getTersedia().equalsIgnoreCase("0"))
        {
            BMulai.setImageResource(R.drawable.button_belumdapatdiambil);
            LLMain.setBackgroundResource(R.drawable.back_laporan2);
        }
        else
        {
            BMulai.setImageResource(R.drawable.button_start);
            LLMain.setBackgroundResource(R.drawable.back_laporan);
        }

        if (!TextUtils.isEmpty(item.getReward()) && item.getStatus().equalsIgnoreCase("2"))
        {
            RLInfo3.setVisibility(View.VISIBLE);
            LInfo3.setText("Dengan hadiah: " + item.getReward());
        }
        else
        {
            RLInfo3.setVisibility(View.GONE);
        }

        if (item.getStatus().equalsIgnoreCase("0"))
        {
            BMulai.setVisibility(View.VISIBLE);
            BLihatLaporan.setVisibility(View.GONE);
        }
        else
        {
            BMulai.setVisibility(View.GONE);
            BLihatLaporan.setVisibility(View.VISIBLE);
        }

        BMulai.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (item.getTersedia().equalsIgnoreCase("0"))
                {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Anda belum dapat mengambil tahapan ini karena tahapan sebelumnya belum diselesaikan")
                            .setCancelable(true)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                            {
                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                                {
                                    dialog.cancel();
                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();
                }
                else
                {
                    Intent intent = new Intent(activity, PersiapanTahapActivity.class);
                    intent.putExtra("tahap", item.getTahap());
                    intent.putExtra("waktu", LInfoWaktu.getText());
                    intent.putExtra("keterangan", item.getKeterangan());
                    activity.startActivity(intent);
                }
            }
        });

        BLihatLaporan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(activity, LaporanTahapActivity.class);
                intent.putExtra("tahap", item.getTahap());
                intent.putExtra("waktu", LInfoWaktu.getText());
                intent.putExtra("status", item.getStatus());
                activity.startActivity(intent);
            }
        });

        BSelesai.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                activity.finish();
                Intent intent = new Intent(activity, SelesaiActivity.class);
                intent.putExtra("point", item.getPoint());
                intent.putExtra("waktu", item.getWaktu());
                activity.startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
