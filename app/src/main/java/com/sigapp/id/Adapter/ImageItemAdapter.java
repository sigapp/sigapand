package com.sigapp.id.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sigapp.id.App.SigapApp;
import com.sigapp.id.Data.ImageItem;
import com.sigapp.id.R;

import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 30 Agustus 2016.
 */
public class ImageItemAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<ImageItem> items;

    public ImageItemAdapter(Activity activity, List<ImageItem> items)
    {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount()
    {
        return items.size();
    }

    @Override
    public Object getItem(int position)
    {
        return items.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.image_item, null);

        ImageView IVGambar = (ImageView) convertView.findViewById(R.id.IVGambar);

        ImageItem item = items.get(position);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SigapApp.getTinggi_layar() / 4);
        layoutParams.setMargins(10, 10, 10, 10);

        IVGambar.setLayoutParams(layoutParams);

        IVGambar.setImageResource(item.getDrawable());

        return convertView;
    }
}
