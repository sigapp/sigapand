package com.sigapp.id.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.toolbox.ImageLoader;
import com.sigapp.id.App.AppController;
import com.sigapp.id.App.LaporanImageView;
import com.sigapp.id.App.SigapApp;
import com.sigapp.id.App.TanggalHandler;
import com.sigapp.id.Data.LaporanItem;
import com.sigapp.id.FotoActivity;
import com.sigapp.id.MapsActivity;
import com.sigapp.id.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.util.List;

/**
 * Created by Aziz Nur Ariffianto on 28 Agustus 2016.
 */
public class LaporanItemAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<LaporanItem> items;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public LaporanItemAdapter(Activity activity, List<LaporanItem> items)
    {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount()
    {
        return items.size();
    }

    @Override
    public Object getItem(int position)
    {
        return items.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.laporan_item, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        ImageView IVJudul = (ImageView) convertView.findViewById(R.id.IVJudul);
        ImageView BReview = (ImageView) convertView.findViewById(R.id.BReview);
        ImageView BBagikan = (ImageView) convertView.findViewById(R.id.BBagikan);
        final LaporanImageView IVLaporan = (LaporanImageView) convertView.findViewById(R.id.IVLaporan);
        final VideoView VVLaporan = (VideoView) convertView.findViewById(R.id.VVLaporan);
        TextView LWaktu = (TextView) convertView.findViewById(R.id.LWaktu);
        TextView LJudul = (TextView) convertView.findViewById(R.id.LJudul);
        TextView LIsi = (TextView) convertView.findViewById(R.id.LIsi);
        TextView LLokasi = (TextView) convertView.findViewById(R.id.LLokasi);

        final LaporanItem item = items.get(position);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SigapApp.getTinggi_layar() / 4);
        layoutParams.setMargins(4, 0, 4, 5);

        IVLaporan.setLayoutParams(layoutParams);
        VVLaporan.setLayoutParams(layoutParams);

        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(SigapApp.getLebar_layar() / 2, 45);
        layoutParams2.setMargins(0, 0, 0, 5);

        IVJudul.setLayoutParams(layoutParams2);

        IVJudul.setImageResource(SigapApp.getJudul()[Integer.valueOf(item.getTahap()) - 1]);
        LWaktu.setText(TanggalHandler.KonversiTanggal(item.getWaktu(), true));
        LJudul.setText(item.getJudul());
        LIsi.setText(item.getIsi());
        if (!TextUtils.isEmpty(item.getAlamat()))
        {
            LLokasi.setText(item.getAlamat());
        }
        else
        {
            LLokasi.setText(item.getLat() + ", " + item.getLing());
        }

        if (item.getJenismedia().equalsIgnoreCase("0"))
        {
            VVLaporan.setVisibility(View.GONE);
            IVLaporan.setImageUrl(item.getMedia(), imageLoader);
            IVLaporan.setVisibility(View.VISIBLE);
            IVLaporan.setResponseObserver(new LaporanImageView.ResponseObserver() {
                @Override
                public void onError() {
                }

                @Override
                public void onSuccess() {
                }
            });
        }
        else
        {
            IVLaporan.setVisibility(View.GONE);
            MediaController mediacontroller = new MediaController(activity);
            mediacontroller.setAnchorView(VVLaporan);
            Uri video = Uri.parse(item.getMedia());
            VVLaporan.setMediaController(mediacontroller);
            VVLaporan.setVideoURI(video);
        }

        IVLaporan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(activity.getApplicationContext(), FotoActivity.class);
                intent.putExtra("url", item.getMedia());
                intent.putExtra("judul", item.getJudul());
                activity.startActivity(intent);
            }
        });

        VVLaporan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                VVLaporan.start();
            }
        });

        LLokasi.setOnClickListener(new TextView.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(activity, MapsActivity.class);
                String[] ll = {item.getLat(), item.getLing()};
                i.putExtra("latling", ll);
                activity.startActivity(i);
            }
        });
        BBagikan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SigapApp.Bagikan(activity, item.getJudul(), item.getIsi(), item.getMedia());
            }
        });

        BReview.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(activity.getApplicationContext(), FotoActivity.class);
                intent.putExtra("url", item.getMedia());
                intent.putExtra("judul", item.getJudul());
                activity.startActivity(intent);
            }
        });

        return convertView;
    }
}
