package com.sigapp.id;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sigapp.id.App.SigapApp;

public class Splash extends AppCompatActivity
{
    public SigapApp sigapApp;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        sigapApp = new SigapApp(getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);

    }
}
