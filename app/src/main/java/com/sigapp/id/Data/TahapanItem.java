package com.sigapp.id.Data;

/**
 * Created by Aziz Nur Ariffianto on 26 Agustus 2016.
 */
public class TahapanItem
{
    String tahap, waktuambil, namatahap, waktu, reward, status, sisawaktu, keterangan, tersedia, point;
    boolean selesai;

    public String getPoint()
    {
        return point;
    }

    public void setPoint(String point)
    {
        this.point = point;
    }

    public String getTersedia()
    {
        return tersedia;
    }

    public void setTersedia(String tersedia)
    {
        this.tersedia = tersedia;
    }

    public boolean isSelesai()
    {
        return selesai;
    }

    public void setSelesai(boolean selesai)
    {
        this.selesai = selesai;
    }

    public String getTahap()
    {
        return tahap;
    }

    public void setTahap(String tahap)
    {
        this.tahap = tahap;
    }

    public String getWaktuambil()
    {
        return waktuambil;
    }

    public void setWaktuambil(String waktuambil)
    {
        this.waktuambil = waktuambil;
    }

    public String getNamatahap()
    {
        return namatahap;
    }

    public void setNamatahap(String namatahap)
    {
        this.namatahap = namatahap;
    }

    public String getWaktu()
    {
        return waktu;
    }

    public void setWaktu(String waktu)
    {
        this.waktu = waktu;
    }

    public String getReward()
    {
        return reward;
    }

    public void setReward(String reward)
    {
        this.reward = reward;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getSisawaktu()
    {
        return sisawaktu;
    }

    public void setSisawaktu(String sisawaktu)
    {
        this.sisawaktu = sisawaktu;
    }

    public String getKeterangan()
    {
        return keterangan;
    }

    public void setKeterangan(String keterangan)
    {
        this.keterangan = keterangan;
    }
}
