package com.sigapp.id.Data;

/**
 * Created by Aziz Nur Ariffianto on 27 Agustus 2016.
 */
public class LaporanItem
{
    String lpid, oleh, tahap, judul, isi, lat, ling, alamat, waktu, media, jenismedia;

    public String getLpid()
    {
        return lpid;
    }

    public void setLpid(String lpid)
    {
        this.lpid = lpid;
    }

    public String getOleh()
    {
        return oleh;
    }

    public void setOleh(String oleh)
    {
        this.oleh = oleh;
    }

    public String getTahap()
    {
        return tahap;
    }

    public void setTahap(String tahap)
    {
        this.tahap = tahap;
    }

    public String getJudul()
    {
        return judul;
    }

    public void setJudul(String judul)
    {
        this.judul = judul;
    }

    public String getIsi()
    {
        return isi;
    }

    public void setIsi(String isi)
    {
        this.isi = isi;
    }

    public String getLat()
    {
        return lat;
    }

    public void setLat(String lat)
    {
        this.lat = lat;
    }

    public String getLing()
    {
        return ling;
    }

    public void setLing(String ling)
    {
        this.ling = ling;
    }

    public String getAlamat()
    {
        return alamat;
    }

    public void setAlamat(String alamat)
    {
        this.alamat = alamat;
    }

    public String getWaktu()
    {
        return waktu;
    }

    public void setWaktu(String waktu)
    {
        this.waktu = waktu;
    }

    public String getMedia()
    {
        return media;
    }

    public void setMedia(String media)
    {
        this.media = media;
    }

    public String getJenismedia()
    {
        return jenismedia;
    }

    public void setJenismedia(String jenismedia)
    {
        this.jenismedia = jenismedia;
    }
}
