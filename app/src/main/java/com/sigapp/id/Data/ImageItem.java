package com.sigapp.id.Data;

/**
 * Created by Aziz Nur Ariffianto on 30 Agustus 2016.
 */
public class ImageItem
{
    int id;
    int drawable;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getDrawable()
    {
        return drawable;
    }

    public void setDrawable(int drawable)
    {
        this.drawable = drawable;
    }
}
