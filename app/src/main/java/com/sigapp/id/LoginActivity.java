package com.sigapp.id;

import android.app.ProgressDialog;
import android.app.backup.FullBackupDataOutput;
import android.content.Context;
import android.content.Intent;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sigapp.id.App.Animasi;
import com.sigapp.id.App.Crypt;
import com.sigapp.id.App.RequestHandler;
import com.sigapp.id.App.SigapApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity
{
    private EditText TBNama, TBEmail, TBUsername, TBTtl, TBAlamat, TBOrganisasi, TBPassword, TBPasswordRegister, TBNamaCofas, TBTtlCofas, TBAlamatCofas, TBOrganisasiCofas, TBNamaDesa, TBKecamatanDesa, TBKabupatenDesa, TBProvinsiDesa, TBPendudukDesa;
    private static EditText TBLokasiDesa;
    private Button BSubmit, BSubmitRegister, BSubmitCofas, BSubmitDDesa;
    private TextView BSignin, BRegister, LForgotPassword;
    private LinearLayout LLBase, LLSignIn, LLRegister, LLJudul, LLCofas, LLDDesa;
    private ImageView BAmbilLokasi;
    private static String cofas, datadesa, alamat, lat, ling, fungsi;

    private int halaman = 0;

    public static void AturLokasiDesa(String valamat, String vlat, String vling)
    {
        alamat = valamat;
        lat = vlat;
        ling = vling;

        if (!TextUtils.isEmpty(valamat))
        {
            TBLokasiDesa.setText(valamat);
        }
        else
        {
            TBLokasiDesa.setText("Lat.: " + lat + ", Ling.: " + ling);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        Intent intent = getIntent();
        fungsi = intent.getStringExtra("fungsi");

        if (fungsi == null)
        {
            fungsi = "DEFAULT";
        }

        TBNama = (EditText) findViewById(R.id.TBNama);
        TBEmail = (EditText) findViewById(R.id.TBEmail);
        TBUsername = (EditText) findViewById(R.id.TBUsername);
        TBTtl = (EditText) findViewById(R.id.TBTtl);
        TBAlamat = (EditText) findViewById(R.id.TBAlamat);
        TBOrganisasi = (EditText) findViewById(R.id.TBOrganisasi);
        TBPassword = (EditText) findViewById(R.id.TBPassword);
        TBPasswordRegister = (EditText) findViewById(R.id.TBPasswordRegister);
        TBNamaCofas = (EditText) findViewById(R.id.TBNamaCofas);
        TBTtlCofas = (EditText) findViewById(R.id.TBTtlCofas);
        TBAlamatCofas = (EditText) findViewById(R.id.TBAlamatCofas);
        TBOrganisasiCofas = (EditText) findViewById(R.id.TBOrganisasiCofas);
        TBNamaDesa = (EditText) findViewById(R.id.TBNamaDesa);
        TBKecamatanDesa = (EditText) findViewById(R.id.TBKecamatanDesa);
        TBKabupatenDesa = (EditText) findViewById(R.id.TBKabupatenDesa);
        TBProvinsiDesa = (EditText) findViewById(R.id.TBProvinsiDesa);
        TBPendudukDesa = (EditText) findViewById(R.id.TBPendudukDesa);
        TBLokasiDesa = (EditText) findViewById(R.id.TBLokasiDesa);

        BSubmit = (Button) findViewById(R.id.BSubmit);
        BSubmitRegister = (Button) findViewById(R.id.BSubmitRegister);
        BSubmitCofas = (Button) findViewById(R.id.BSubmitCofas);
        BSubmitDDesa = (Button) findViewById(R.id.BSubmitDDesa);

        BSignin = (TextView) findViewById(R.id.BSignIn);
        BRegister = (TextView) findViewById(R.id.BRegister);
        LForgotPassword = (TextView) findViewById(R.id.LForgotPassword);

        BSignin.setTextSize((SigapApp.getLebar_layar() / SigapApp.getDpi()) * 10);
        BRegister.setTextSize((SigapApp.getLebar_layar() / SigapApp.getDpi()) * 10);

        LLBase = (LinearLayout) findViewById(R.id.LLBase);
        LLSignIn = (LinearLayout) findViewById(R.id.LLSignIn);
        LLRegister = (LinearLayout) findViewById(R.id.LLRegister);
        LLJudul = (LinearLayout) findViewById(R.id.LLJudul);
        LLCofas = (LinearLayout) findViewById(R.id.LLCofas);
        LLDDesa = (LinearLayout) findViewById(R.id.LLDDesa);

        BAmbilLokasi = (ImageView) findViewById(R.id.BAmbilLokasi);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SigapApp.getTinggi_layar() / 9);
        LLJudul.setLayoutParams(layoutParams);

        BAmbilLokasi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), AmbilLokasiActivity.class);
                startActivity(intent);
            }
        });

        LForgotPassword.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), LupaPasswordActivity.class);
                startActivity(intent);
            }
        });

        BRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (halaman != 1)
                {
                    LLBase.setBackgroundResource(R.drawable.back_register);
                    //Animasi.Fade(LLBase, 200, R.drawable.back_register);
                    Animasi.Fade(LLSignIn, LLRegister, 200);
                    BSignin.setTextColor(Color.WHITE);
                    BRegister.setTextColor(Color.BLACK);
                    halaman = 1;
                }
            }
        });

        BSignin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (halaman != 0)
                {
                    LLBase.setBackgroundResource(R.drawable.back_signin);
                    //Animasi.Fade(LLBase, 200, R.drawable.back_signin);
                    Animasi.Fade(LLRegister, LLSignIn, 200);
                    BSignin.setTextColor(Color.BLACK);
                    BRegister.setTextColor(Color.WHITE);
                    halaman = 0;
                }
            }
        });

        BSubmitRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isAllValid(0))
                {
                    String[] isi = AmbilIsian(0);
                    new RegisterManager(getApplicationContext()).execute(isi[0],
                            isi[1],
                            isi[2],
                            isi[3],
                            isi[4],
                            isi[5]);
                }
            }
        });

        BSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isAllValid(1))
                {
                    String[] isi = AmbilIsian(1);
                    new LoginManager(getApplicationContext()).execute(isi[0], isi[1]);
                }
            }
        });

        BSubmitCofas.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isAllValid(2))
                {
                    String[] isi = AmbilIsian(2);
                    new CofasManager(getApplicationContext()).execute(isi[0], isi[1], isi[2], isi[3], isi[4]);
                }
            }
        });

        BSubmitDDesa.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isAllValid(3))
                {
                    String[] isi = AmbilIsian(3);
                    new DataDesaManager(getApplicationContext()).execute(isi[0], isi[1], isi[2], isi[3], isi[4], isi[5], isi[6], isi[7], isi[8]);
                }
            }
        });

        if (fungsi.equalsIgnoreCase("LENGKAPI"))
        {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    new LoginManager(getApplicationContext()).execute(SigapApp.getUsername(), SigapApp.getPassword());
                }
            }, 800);
        }
    }

    private boolean isAllValid(int bagian)
    {
        Boolean valid = true;

        if (bagian == 0)
        {
            if (TextUtils.isEmpty(TBNama.getText().toString()))
            {
                TBNama.setError(getString(R.string.error_field_required));
                TBNama.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBAlamat.getText().toString()))
            {
                TBAlamat.setError(getString(R.string.error_field_required));
                TBAlamat.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBTtl.getText().toString()))
            {
                TBTtl.setError(getString(R.string.error_field_required));
                TBTtl.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBOrganisasi.getText().toString()))
            {
                TBOrganisasi.setError(getString(R.string.error_field_required));
                TBOrganisasi.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBEmail.getText().toString()))
            {
                TBEmail.setError(getString(R.string.error_field_required));
                TBEmail.requestFocus();
                valid = false;
            }
            else if (TBEmail.getText().toString().contains("@") == false || TBEmail.getText().toString().contains(".") == false)
            {
                TBEmail.setError(getString(R.string.error_invalid_email));
                TBEmail.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBPasswordRegister.getText().toString()))
            {
                TBPasswordRegister.setError(getString(R.string.error_field_required));
                TBPasswordRegister.requestFocus();
                valid = false;
            }
            else if (!TextUtils.isEmpty(TBPasswordRegister.getText().toString()) && TBPasswordRegister.getText().toString().length() < 5)
            {
                TBPasswordRegister.setError(getString(R.string.error_invalid_password));
                TBPasswordRegister.requestFocus();
                valid = false;
            }
        }
        else if (bagian == 1)
        {
            if (TextUtils.isEmpty(TBUsername.getText().toString()))
            {
                TBUsername.setError(getString(R.string.error_field_required));
                TBUsername.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBPassword.getText().toString()))
            {
                TBPassword.setError(getString(R.string.error_field_required));
                TBPassword.requestFocus();
                valid = false;
            }
        }
        else if (bagian == 2)
        {
            if (TextUtils.isEmpty(TBNamaCofas.getText().toString()))
            {
                TBNamaCofas.setError(getString(R.string.error_field_required));
                TBNamaCofas.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBTtlCofas.getText().toString()))
            {
                TBTtlCofas.setError(getString(R.string.error_field_required));
                TBTtlCofas.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBAlamatCofas.getText().toString()))
            {
                TBAlamatCofas.setError(getString(R.string.error_field_required));
                TBAlamatCofas.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBOrganisasiCofas.getText().toString()))
            {
                TBOrganisasiCofas.setError(getString(R.string.error_field_required));
                TBOrganisasiCofas.requestFocus();
                valid = false;
            }
        }
        else if (bagian == 3)
        {
            if (TextUtils.isEmpty(TBNamaDesa.getText().toString()))
            {
                TBNamaDesa.setError(getString(R.string.error_field_required));
                TBNamaDesa.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBKecamatanDesa.getText().toString()))
            {
                TBKecamatanDesa.setError(getString(R.string.error_field_required));
                TBKecamatanDesa.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBKabupatenDesa.getText().toString()))
            {
                TBKabupatenDesa.setError(getString(R.string.error_field_required));
                TBKabupatenDesa.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBProvinsiDesa.getText().toString()))
            {
                TBProvinsiDesa.setError(getString(R.string.error_field_required));
                TBProvinsiDesa.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBPendudukDesa.getText().toString()))
            {
                TBPendudukDesa.setError(getString(R.string.error_field_required));
                TBPendudukDesa.requestFocus();
                valid = false;
            }
            else if (TextUtils.isEmpty(TBLokasiDesa.getText().toString()))
            {
                TBLokasiDesa.setError(getString(R.string.error_field_required));
                TBLokasiDesa.requestFocus();
                valid = false;
            }
        }

        return valid;
    }

    public String[] AmbilIsian(int bagian)
    {
        if (bagian == 0)
        {
            String[] Isian = {"", "", "", "", "", ""};

            Isian[0] = TBNama.getText().toString();
            Isian[1] = TBEmail.getText().toString();
            Isian[2] = TBTtl.getText().toString();
            Isian[3] = TBAlamat.getText().toString();
            Isian[4] = TBOrganisasi.getText().toString();
            Isian[5] = TBPasswordRegister.getText().toString();

            return  Isian;
        }
        else if (bagian == 1)
        {
            String[] Isian = {"", ""};

            Isian[0] = TBUsername.getText().toString();
            Isian[1] = TBPassword.getText().toString();

            return  Isian;
        }
        else if (bagian == 2)
        {
            String[] Isian = {"", "", "", "", ""};

            Isian[0] = SigapApp.getPgid();
            Isian[1] = TBNamaCofas.getText().toString();
            Isian[2] = TBTtlCofas.getText().toString();
            Isian[3] = TBAlamatCofas.getText().toString();
            Isian[4] = TBOrganisasiCofas.getText().toString();

            return  Isian;
        }
        else if (bagian == 3)
        {
            String[] Isian = {"", "", "", "", "", "", "", "", ""};

            Isian[0] = SigapApp.getPgid();
            Isian[1] = TBNamaDesa.getText().toString();
            Isian[2] = TBKecamatanDesa.getText().toString();
            Isian[3] = TBKabupatenDesa.getText().toString();
            Isian[4] = TBProvinsiDesa.getText().toString();
            Isian[5] = TBPendudukDesa.getText().toString();
            Isian[6] = lat;
            Isian[7] = ling;
            Isian[8] = TBLokasiDesa.getText().toString();

            return  Isian;
        }

        return null;
    }

    ProgressDialog pDialog;

    class RegisterManager extends AsyncTask<String, Void, String>
    {
        private Context context;

        public RegisterManager(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Mendaftarkan...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("nama", arg0[0]);
                data.put("email", arg0[1]);
                data.put("ttl", arg0[2]);
                data.put("alamat", arg0[3]);
                data.put("organisasi", arg0[4]);
                data.put("password", arg0[5]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_pengguna);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            Log.i("RESULT", "onPostExecute: " + result);
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        TBUsername.setText(TBEmail.getText());
                        BSignin.performClick();

                        TBNama.setText("");
                        TBAlamat.setText("");
                        TBTtl.setText("");
                        TBEmail.setText("");
                        TBOrganisasi.setText("");
                        TBPasswordRegister.setText("");

                        TBPassword.requestFocus();
                    } else if (query_result.equals("GAGAL"))
                    {
                        Toast.makeText(context, "Pendaftaran gagal.", Toast.LENGTH_SHORT).show();
                    } else if (query_result.equals("EMAIL TIDAK VALID"))
                    {
                        Toast.makeText(context, "Email sudah dipakai.", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(context, "Tidak dapat terhubung ke database.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Toast.makeText(context, "Tidak dapat mengambil data JSON.", Toast.LENGTH_SHORT).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    class LoginManager extends AsyncTask<String, Void, String> {

        private Context context;

        public LoginManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            if (fungsi.equalsIgnoreCase("LENGKAPI"))
            {
                pDialog.setMessage("Mengecek kekurangan...");
            }
            else
            {
                pDialog.setMessage("Signin...");
            }
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            RequestHandler rh = new RequestHandler();
            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("email", arg0[0]);
                data.put("password", arg0[1]);

                link = getString(R.string.alamat_server) + getString(R.string.link_login_pengguna);
                result = rh.sendPostRequest(link, data);;

                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String hasil = jsonObj.getString("hasil");
                    if (hasil.equals("ADA")) {
                        String pgid = jsonObj.getString("pgid");
                        String nama = jsonObj.getString("nama");
                        String pass = jsonObj.getString("pass");
                        String email = jsonObj.getString("email");
                        String t = jsonObj.getString("t");
                        cofas = jsonObj.getString("cofas");
                        datadesa = jsonObj.getString("datadesa");

                        SigapApp.setPgid(pgid);
                        SigapApp.setNama(nama);
                        SigapApp.setPassword(pass);
                        SigapApp.setUsername(email);
                        SigapApp.setTimecurlaporan(t);
                        if (cofas.equalsIgnoreCase("0"))
                        {
                            SigapApp.setCofas(false);
                        }
                        else
                        {
                            SigapApp.setCofas(true);
                        }
                        if (datadesa.equalsIgnoreCase("0"))
                        {
                            SigapApp.setDesa(false);
                        }
                        else
                        {
                            SigapApp.setDesa(true);
                        }
                        SigapApp.SimpanPengaturan();


                        if (fungsi.equalsIgnoreCase("LOGIN"))
                        {
                            finish();
                        }
                        else
                        {
                            if (cofas.equalsIgnoreCase("0"))
                            {
                                Animasi.Fade(LLBase, 200, R.drawable.back_cofas);
                                Animasi.Fade(LLCofas, true, 200);
                                LLJudul.setVisibility(View.GONE);
                                LLRegister.setVisibility(View.GONE);
                                LLSignIn.setVisibility(View.GONE);
                                LLDDesa.setVisibility(View.GONE);
                            }
                            else if (datadesa.equalsIgnoreCase("0"))
                            {
                                Animasi.Fade(LLBase, 200, R.drawable.back_cofas);
                                Animasi.Fade(LLDDesa, true, 200);
                                LLJudul.setVisibility(View.GONE);
                                LLRegister.setVisibility(View.GONE);
                                LLSignIn.setVisibility(View.GONE);
                                LLCofas.setVisibility(View.GONE);
                            }
                            else
                            {
                                finish();
                                Intent intent = new Intent(getApplicationContext(), TahapActivity.class);
                                startActivity(intent);
                            }
                        }
                    } else if (hasil.equals("TIDAK ADA")) {
                        Snackbar.make(LLBase, "Email / No.Telp dan Password tidak valid.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    } else {
                        Snackbar.make(LLBase, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                } catch (JSONException e)
                {
                    Snackbar.make(LLBase, "Ditemukan error. " + e.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            } else
            {
                Snackbar.make(LLBase, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    class CofasManager extends AsyncTask<String, Void, String> {

        private Context context;

        public CofasManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Mengirim...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            RequestHandler rh = new RequestHandler();
            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);
                data.put("nama", arg0[1]);
                data.put("ttl", arg0[2]);
                data.put("alamat", arg0[3]);
                data.put("organisasi", arg0[4]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_cofas);
                result = rh.sendPostRequest(link, data);;

                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String hasil = jsonObj.getString("hasil");
                    String datadesa = jsonObj.getString("datadesa");
                    if (hasil.equals("SUKSES"))
                    {
                        SigapApp.setCofas(true);
                        SigapApp.SimpanPengaturan();
                        if (datadesa.equalsIgnoreCase("0"))
                        {
                            Animasi.Fade(LLBase, 200, R.drawable.back_cofas);
                            Animasi.Fade(LLDDesa, true, 200);
                            LLJudul.setVisibility(View.GONE);
                            LLRegister.setVisibility(View.GONE);
                            LLSignIn.setVisibility(View.GONE);
                            LLCofas.setVisibility(View.GONE);
                        }
                        else
                        {
                            finish();
                            Intent intent = new Intent(getApplicationContext(), TahapActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        Snackbar.make(LLBase, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                } catch (JSONException e)
                {
                    Log.i("EROR", "onPostExecute: " + result);
                    Snackbar.make(LLBase, "Ditemukan error. " + e.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            } else
            {
                Snackbar.make(LLBase, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    class DataDesaManager extends AsyncTask<String, Void, String> {

        private Context context;

        public DataDesaManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Mengirim...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            RequestHandler rh = new RequestHandler();
            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);
                data.put("namadesa", arg0[1]);
                data.put("kecamatan", arg0[2]);
                data.put("kabupaten", arg0[3]);
                data.put("provinsi", arg0[4]);
                data.put("jumlahpenduduk", arg0[5]);
                data.put("lat", arg0[6]);
                data.put("ling", arg0[7]);
                data.put("alamat", arg0[8]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_desa);
                result = rh.sendPostRequest(link, data);;

                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String hasil = jsonObj.getString("hasil");
                    if (hasil.equals("SUKSES")) {
                        SigapApp.setDesa(true);
                        SigapApp.SimpanPengaturan();

                        finish();
                        Intent intent = new Intent(getApplicationContext(), TahapActivity.class);
                        startActivity(intent);
                    } else {
                        Snackbar.make(LLBase, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                } catch (JSONException e)
                {
                    Log.i("EROR", "onPostExecute: " + result);
                    Snackbar.make(LLBase, "Ditemukan error. " + e.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            } else
            {
                Snackbar.make(LLBase, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}
