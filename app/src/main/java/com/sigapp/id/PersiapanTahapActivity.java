package com.sigapp.id;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sigapp.id.App.RequestHandler;
import com.sigapp.id.App.SigapApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PersiapanTahapActivity extends AppCompatActivity
{
    String tahap, waktu, keterangan;
    private ImageView BLihatEbooks;

    int[] judul = { R.drawable.gambar_tahap1,
            R.drawable.gambar_tahap2,
            R.drawable.gambar_tahap3,
            R.drawable.gambar_tahap4,
            R.drawable.gambar_tahap5,
            R.drawable.gambar_tahap6,
            R.drawable.gambar_tahap7};

    int[] ilustrasi = { R.drawable.icon_tahap1,
            R.drawable.icon_tahap2,
            R.drawable.icon_tahap3,
            R.drawable.icon_tahap4,
            R.drawable.icon_tahap5,
            R.drawable.icon_tahap6,
            R.drawable.icon_tahap7};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persiapan_tahap);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        ImageView IVJudul = (ImageView) findViewById(R.id.IVJudul);
        ImageView IVIlustrasi = (ImageView) findViewById(R.id.IVIlustrasi);
        BLihatEbooks = (ImageView) findViewById(R.id.BLihatEbooks);
        ImageView BStart = (ImageView) findViewById(R.id.BStart);

        TextView LInfoWaktu = (TextView) findViewById(R.id.LInfoWaktu);
        TextView LKeterangan = (TextView) findViewById(R.id.LKeterangan);

        Intent intent = getIntent();
        tahap = intent.getStringExtra("tahap");
        waktu = intent.getStringExtra("waktu");
        keterangan = intent.getStringExtra("keterangan");

        IVJudul.setImageResource(judul[Integer.valueOf(tahap) - 1]);
        IVIlustrasi.setImageResource(ilustrasi[Integer.valueOf(tahap) - 1]);
        LKeterangan.setText(keterangan);
        LInfoWaktu.setText(waktu);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SigapApp.getTinggi_layar() / 2);
        layoutParams.setMargins(10, 10, 10, 10);

        IVIlustrasi.setLayoutParams(layoutParams);

        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(SigapApp.getLebar_layar() / 2, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams2.setMargins(10, 0, 0, 5);

        IVJudul.setLayoutParams(layoutParams2);

        BLihatEbooks.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), IsiEBooksActivity.class);
                intent.putExtra("halaman", Integer.valueOf(tahap));
                startActivity(intent);
            }
        });

        BStart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final AlertDialog.Builder builder = new AlertDialog.Builder(PersiapanTahapActivity.this);
                builder.setMessage("Apa Anda yakin ingin memulai tahapan ini ?")
                        .setCancelable(true)
                        .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                        {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                            {
                                new AmbilTahapManager(getApplicationContext()).execute(SigapApp.getPgid(), tahap);
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                        {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    class AmbilTahapManager extends AsyncTask<String, Void, String>
    {
        private ProgressDialog pDialog;
        private Context context;

        public AmbilTahapManager(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PersiapanTahapActivity.this);
            pDialog.setMessage("Memproses...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            RequestHandler rh = new RequestHandler();
            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);
                data.put("tahap", arg0[1]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_ambil_tahap);
                result = rh.sendPostRequest(link, data);;

                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String hasil = jsonObj.getString("hasil");
                    if (hasil.equals("SUKSES")) {
                        finish();
                        Intent intent = new Intent(getApplicationContext(), LaporanTahapActivity.class);
                        intent.putExtra("tahap", tahap);
                        intent.putExtra("waktu", waktu);
                        intent.putExtra("status", "1");
                        startActivity(intent);
                    } else {
                        Snackbar.make(BLihatEbooks, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                } catch (JSONException e)
                {
                    Log.i("EROR", "onPostExecute: " + result);
                    Snackbar.make(BLihatEbooks, "Ditemukan error. " + e.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            } else
            {
                Snackbar.make(BLihatEbooks, "Tidak dapat terhubung ke database. " + jsonStr.toString(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
}
