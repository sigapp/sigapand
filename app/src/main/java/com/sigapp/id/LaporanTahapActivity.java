package com.sigapp.id;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Config;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sigapp.id.Adapter.LaporanItemAdapter;
import com.sigapp.id.App.AppController;
import com.sigapp.id.App.SigapApp;
import com.sigapp.id.Data.LaporanItem;
import com.sigapp.id.Data.TahapanItem;
import com.twotoasters.jazzylistview.JazzyListView;
import com.twotoasters.jazzylistview.effects.TiltEffect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LaporanTahapActivity extends AppCompatActivity
{
    private ImageView BTambahLaporan;
    private JazzyListView LVLaporan;
    private TextView LInfoWaktu, LKeterangan;
    private LaporanItemAdapter listAdapter;
    private List<LaporanItem> items;
    private String url, tahap, waktu, status;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private Uri fileUri;

    private String TAG = "%#^#@%$^#@%$";

    private void ambilFoto()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void ambilVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK)
            {
                launchUploadActivity(true);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Pengambilan foto dibatalkan", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(),"Tidak dapat mengambil foto", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK)
            {
                launchUploadActivity(false);
            } else if (resultCode == RESULT_CANCELED)
            {

                Toast.makeText(getApplicationContext(), "Pengambilan video dibatalkan", Toast.LENGTH_SHORT).show();

            } else
            {
                Toast.makeText(getApplicationContext(), "Tidak dapat mengambil video", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void launchUploadActivity(boolean isImage){
        Intent i = new Intent(LaporanTahapActivity.this, TambahLaporanActivity.class);
        i.putExtra("filePath", fileUri.getPath());
        i.putExtra("isImage", isImage);
        i.putExtra("tahap", tahap);
        startActivity(i);
    }

    public void refreshContent(final boolean daricache)
    {
        url = getString(R.string.alamat_server) + getString(R.string.link_ambil_laporan) + "?oleh=" + SigapApp.getPgid() + "&tahap=" + tahap + "&all=0";
        url = Uri.encode(url, SigapApp.KARAKTER_URI);
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        if (entry != null && daricache) {
            try {
                String data = new String(entry.data, "UTF-8");
                try
                {
                    parse(new JSONObject(data));
                } catch (JSONException e)
                {
                    mSwipeRefreshLayout.setRefreshing(false);
                    refreshContent(false);
                }
            } catch (UnsupportedEncodingException e) {
                mSwipeRefreshLayout.setRefreshing(false);
                refreshContent(false);
            }

        } else {
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    if (response != null) {
                        items.clear();
                        parse(response);
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try
                    {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    catch (Exception e)
                    {
                        refreshContent(false);
                    }
                }
            });

            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    private void parse(JSONObject response)
    {
        try {
            LKeterangan.setVisibility(View.GONE);
            JSONArray feedArray = response.getJSONArray("laporan");

            for (int i = 0; i < feedArray.length(); i++)
            {
                JSONObject laporan = (JSONObject) feedArray.get(i);

                final LaporanItem item = new LaporanItem();
                item.setLpid(laporan.getString("a"));
                item.setOleh(laporan.getString("b"));
                item.setTahap(laporan.getString("c"));
                item.setJudul(laporan.getString("d"));
                item.setIsi(laporan.getString("e"));
                item.setLat(laporan.getString("f"));
                item.setLing(laporan.getString("g"));
                item.setAlamat(laporan.getString("h"));
                item.setWaktu(laporan.getString("i"));
                item.setMedia(laporan.getString("j"));
                item.setJenismedia(laporan.getString("k"));

                items.add(item);
            }

            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            LKeterangan.setVisibility(View.VISIBLE);
            LKeterangan.setText("Tidak ada laporan...");
            items.clear();
            listAdapter.notifyDataSetChanged();
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_tahap);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        Intent intent = getIntent();
        tahap = intent.getStringExtra("tahap");
        waktu = intent.getStringExtra("waktu");
        status = intent.getStringExtra("status");

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.parseColor("#35BC7A"));
        mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#FFFFFFFF"));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent(false);
            }
        });

        LVLaporan = (JazzyListView) findViewById(R.id.LVLaporan);
        BTambahLaporan = (ImageView) findViewById(R.id.BTambahLaporan);
        LInfoWaktu = (TextView) findViewById(R.id.LInfoWaktu);
        LKeterangan = (TextView) findViewById(R.id.LKeterangan);

        LVLaporan.setTransitionEffect(new TiltEffect());

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(500);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        LVLaporan.setLayoutAnimation(controller);

        LInfoWaktu.setText(waktu);

        if (status.equalsIgnoreCase("2"))
        {
            BTambahLaporan.setVisibility(View.GONE);
        }

        BTambahLaporan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!isDeviceSupportCamera()) {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Your device doesn't support camera",
                            Toast.LENGTH_LONG).show();
                    finish();
                }
                else
                {
                    final AlertDialog.Builder builder2 = new AlertDialog.Builder(LaporanTahapActivity.this);
                    builder2.setMessage("Media apa yang ingin Anda gunakan ?")
                            .setCancelable(true)
                            .setPositiveButton("Foto", new DialogInterface.OnClickListener() {
                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                {
                                    ambilFoto();
                                }
                            })
                            .setNegativeButton("Video", new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                    ambilVideo();
                                }
                            });
                    final AlertDialog alert2 = builder2.create();
                    alert2.show();
                }
            }
        });

        items = new ArrayList<LaporanItem>();
        listAdapter = new LaporanItemAdapter(this, items);
        LVLaporan.setAdapter(listAdapter);
    }

    @Override
    protected void onResume()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {
                refreshContent(false);
            }
        }, 500);
        super.onResume();
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type)
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Sigapp/Laporan");

        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
}
