package com.sigapp.id;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sigapp.id.App.SigapApp;

public class IsiEBooksActivity extends AppCompatActivity
{
    TextView LJudul1,
             LJudul2,
             LJudul3,
             LJudul4,
             LJudul5,
             LSubJudul1,
             LIsi1,
             LIsi2,
             LIsi3,
             LIsi4,
             LIsi5;

    private Toolbar toolbar;
    private float ukuranjudul, ukuransubjudul, ukuranisi;

    private void AturIsi(int halaman)
    {
        LJudul1 = (TextView) findViewById(R.id.LJudul1);
        LJudul2 = (TextView) findViewById(R.id.LJudul2);
        LJudul3 = (TextView) findViewById(R.id.LJudul3);
        LJudul4 = (TextView) findViewById(R.id.LJudul4);
        LJudul5 = (TextView) findViewById(R.id.LJudul5);
        LSubJudul1 = (TextView) findViewById(R.id.LSubJudul1);
        LIsi1 = (TextView) findViewById(R.id.LIsi1);
        LIsi2 = (TextView) findViewById(R.id.LIsi2);
        LIsi3 = (TextView) findViewById(R.id.LIsi3);
        LIsi4 = (TextView) findViewById(R.id.LIsi4);
        LIsi5 = (TextView) findViewById(R.id.LIsi5);

        ukuranjudul = LJudul1.getTextSize();
        ukuransubjudul = LSubJudul1.getTextSize();
        ukuranisi = LIsi1.getTextSize();

        switch (halaman)
        {
            case 0:
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("PENGANTAR");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.pengantar));

                LJudul2.setVisibility(View.VISIBLE);
                LJudul2.setText("PERAN INDONESIA DALAM MENGATASI PERUBAHAN IKLIM: REDD, REDD+ DAN PKHB");
                LIsi2.setVisibility(View.VISIBLE);
                LIsi2.setText(getString(R.string.peran_indonesia_perubahan_iklim));

                LJudul3.setVisibility(View.VISIBLE);
                LJudul3.setText("MEMASTIKAN MASYARAKAT TERLIBAT DAN MENDAPAT MANFAAT");
                LIsi3.setVisibility(View.VISIBLE);
                LIsi3.setText(getString(R.string.memastikan_masyarakat));

                LJudul4.setVisibility(View.VISIBLE);
                LJudul4.setText("SIGAP: AKSI INSPIRATIF WARGA UNTUK PERUBAHAN");
                LIsi4.setVisibility(View.VISIBLE);
                LIsi4.setText(getString(R.string.sigap_aksi_inspiratif));

                LJudul5.setVisibility(View.VISIBLE);
                LJudul5.setText("7D: TUJUH TAHAPAN PELAKSANAAN SIGAP");
                LIsi5.setVisibility(View.VISIBLE);
                LIsi5.setText(getString(R.string.tujuh_tahapan_pelaksanaan));
                break;
            case 1:
                LJudul1.setTextColor(Color.parseColor("#FFF16523"));
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("DISCLOSURE");
                LSubJudul1.setVisibility(View.VISIBLE);
                LSubJudul1.setText("DEKATKAN DIRI, HATI, DAN PIKIRAN");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.disclosure));
                break;
            case 2:
                LJudul1.setTextColor(Color.CYAN);
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("DEFINE");
                LSubJudul1.setVisibility(View.VISIBLE);
                LSubJudul1.setText("DIALOGKAN TEMA PERUBAHAN");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.define));
                break;
            case 3:
                LJudul1.setTextColor(Color.parseColor("#FF8560AA"));
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("DISCOVERY");
                LSubJudul1.setVisibility(View.VISIBLE);
                LSubJudul1.setText("DAPATKAN KEKUATAN");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.discovery));
                break;
            case 4:
                LJudul1.setTextColor(Color.parseColor("#FF6E441B"));
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("DREAM");
                LSubJudul1.setVisibility(View.VISIBLE);
                LSubJudul1.setText("DEKLARASIKAN IMPIAN");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.dream));
                break;
            case 5:
                LJudul1.setTextColor(Color.parseColor("#FFF16523"));
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("DESIGN");
                LSubJudul1.setVisibility(View.VISIBLE);
                LSubJudul1.setText("DETILKAN RENCANA PERUBAHAN");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.design));
                break;
            case 6:
                LJudul1.setTextColor(Color.RED);
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("DELIVERY");
                LSubJudul1.setVisibility(View.VISIBLE);
                LSubJudul1.setText("DAYAUPAYAKAN PERUBAHAN");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.delivery));
                break;
            case 7:
                LJudul1.setTextColor(Color.parseColor("#FF8560AA"));
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("DRIVE");
                LSubJudul1.setVisibility(View.VISIBLE);
                LSubJudul1.setText("DENGUNGKAN KEBERHASILAN");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.drive));
                break;
            case 8:
                LJudul1.setTextColor(Color.parseColor("#FF21B472"));
                LJudul1.setVisibility(View.VISIBLE);
                LJudul1.setText("PENUTUP");
                LIsi1.setVisibility(View.VISIBLE);
                LIsi1.setText(getString(R.string.penutup));
                break;
        }
    }

    private void AturUkuran()
    {
        LJudul1.setTextSize(ukuranjudul + SigapApp.getOffsetukuranhuruf());
        LJudul2.setTextSize(ukuranjudul + SigapApp.getOffsetukuranhuruf());
        LJudul3.setTextSize(ukuranjudul + SigapApp.getOffsetukuranhuruf());
        LJudul4.setTextSize(ukuranjudul + SigapApp.getOffsetukuranhuruf());
        LJudul5.setTextSize(ukuranjudul + SigapApp.getOffsetukuranhuruf());
        LSubJudul1.setTextSize(ukuransubjudul + SigapApp.getOffsetukuranhuruf());
        LIsi1.setTextSize(ukuranisi + SigapApp.getOffsetukuranhuruf());
        LIsi2.setTextSize(ukuranisi + SigapApp.getOffsetukuranhuruf());
        LIsi3.setTextSize(ukuranisi + SigapApp.getOffsetukuranhuruf());
        LIsi4.setTextSize(ukuranisi + SigapApp.getOffsetukuranhuruf());
        LIsi5.setTextSize(ukuranisi + SigapApp.getOffsetukuranhuruf());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isiebooks);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
        });

        Intent intent = getIntent();
        halaman = intent.getIntExtra("halaman", 0);
        AturIsi(halaman);
        AturUkuran();
    }

    int halaman;

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
        return;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_isiebooks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_perbesar)
        {
            SigapApp.setOffsetukuranhuruf(SigapApp.getOffsetukuranhuruf() + 1f);
            SigapApp.SimpanPengaturan();
            AturUkuran();
        }
        else if (id == R.id.action_perkecil)
        {
            SigapApp.setOffsetukuranhuruf(SigapApp.getOffsetukuranhuruf() - 1f);
            SigapApp.SimpanPengaturan();
            AturUkuran();
        }
        return super.onOptionsItemSelected(item);
    }
}
