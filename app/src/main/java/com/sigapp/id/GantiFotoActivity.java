package com.sigapp.id;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.sigapp.id.App.AndroidMultiPartEntity;
import com.sigapp.id.App.AppController;
import com.sigapp.id.App.ImageHandler;
import com.sigapp.id.App.RealPath;
import com.sigapp.id.App.RequestHandler;
import com.sigapp.id.App.SigapApp;
import com.sigapp.id.App.TouchImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class GantiFotoActivity extends AppCompatActivity
{
    TouchImageView TIFoto;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    ImageLoader.ImageContainer newContainer;
    String url, namafile;
    long totalSize = 0;
    String ff;

    private String filePath;
    private Bitmap Gambar;

    private static int KODE_AMBIL_FOTO = 1;
    private static int KODE_PILIH_FOTO = 2;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private boolean batalkan;

    private void AmbilFoto(String link)
    {
        TIFoto.setMaxZoom(3);

        newContainer = imageLoader.get(link, new ImageLoader.ImageListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                TIFoto.setImageResource(R.drawable.memuat);
            }

            @Override
            public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate)
            {
                if (response.getBitmap() != null)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
                    {
                        TIFoto.animate().setDuration(300).alpha(0).setListener(new AnimatorListenerAdapter() {
                            @SuppressLint("NewApi")
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                TIFoto.setImageBitmap(response.getBitmap());
                                TIFoto.animate().setDuration(250).alpha(1).setListener(new AnimatorListenerAdapter()
                                {
                                    @Override
                                    public void onAnimationEnd(Animator animation)
                                    {
                                    }
                                });
                            }
                        });
                    }
                    else
                    {
                        TIFoto.setImageBitmap(response.getBitmap());
                    }

                }
                else
                {
                    TIFoto.setImageResource(R.drawable.memuat);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gantifoto);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        Intent intent = getIntent();
        url = intent.getStringExtra("url");

        if (imageLoader == null) {
            imageLoader = AppController.getInstance().getImageLoader();
        }

        TIFoto = (TouchImageView) findViewById(R.id.TIFoto);

        namafile = url.substring(url.lastIndexOf('/') + 1);
        AmbilFoto(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gantifoto, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_gantifoto)
        {
            final AlertDialog.Builder builder2 = new AlertDialog.Builder(GantiFotoActivity.this);
            builder2.setMessage("Lewat apa Anda ingin memilih foto ?")
                    .setCancelable(true)
                    .setPositiveButton("Galeri", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                        {
                            openGalerry();
                        }
                    })
                    .setNegativeButton("Kamera", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                        {
                            openCamera();
                        }
                    });
            final AlertDialog alert2 = builder2.create();
            alert2.show();
        }
        else if (id == R.id.action_simpan)
        {
            Bitmap bitmap = ((BitmapDrawable) TIFoto.getDrawable()).getBitmap();
            saveImageToInternalStorage(bitmap);
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean saveImageToInternalStorage(Bitmap image) {

        try {
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            File myDir = new File(root + "/Sigapp/Foto Pengguna");

            File file = new File(myDir, namafile);

            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            if (file.exists())
            {
                file.delete();
            }

            FileOutputStream fos = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();

            Toast.makeText(getApplicationContext(), "Foto telah disimpan di Pustaka", Toast.LENGTH_SHORT).show();
            return true;
        } catch (Exception e) {
            Log.e("saveToInternalStorage()", e.getMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private Uri fileUri;

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type)
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "/Sigapp/Foto Pengguna");

        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void openCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, KODE_AMBIL_FOTO);
        }
        else
        {
            Toast.makeText(getApplication(), "Tidak ditemukan adanya kamera.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try
        {
            outState.putParcelable("file_uri", fileUri);
            outState.putString("filePath", filePath);
        }
        catch (Exception e)
        {}
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        try
        {
            fileUri = savedInstanceState.getParcelable("file_uri");
            filePath = savedInstanceState.getParcelable("filePath");
        }
        catch (Exception e)
        {}
    }

    private void openGalerry() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), KODE_PILIH_FOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)
        {
            if (requestCode == KODE_AMBIL_FOTO)
            {
                try
                {
                    Gambar = ImageHandler.decodeSampledBitmapFromFile(fileUri.getPath(), 500, 500);
                    TIFoto.setImageBitmap(Gambar);
                    filePath = fileUri.getPath();
                }
                catch (Exception ex)
                {
                    Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                }
            }
            else if (requestCode == KODE_PILIH_FOTO && data != null && data.getData() != null)
            {
                try
                {
                    Uri pickedImage = data.getData();
                    String[] filePath2 = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(pickedImage, filePath2, null, null, null);
                    cursor.moveToFirst();
                    filePath = cursor.getString(cursor.getColumnIndex(filePath2[0]));

                    Gambar = ImageHandler.decodeSampledBitmapFromFile(filePath, 500, 500);
                    TIFoto.setImageBitmap(Gambar);
                }
                catch (Exception ex)
                {
                    Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                }
            }

            new UploadFileToServer().execute();
        }
    }

    private ProgressDialog progressBar;

    private class UploadFileToServer extends AsyncTask<Void, Integer, String>
    {

        @Override
        protected void onPreExecute() {
            progressBar=new ProgressDialog(GantiFotoActivity.this);
            progressBar.setMessage("Memperbarui Foto...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setIndeterminate(false);
            progressBar.setCancelable(false);
            progressBar.show();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            progressBar.setProgress((int) ((progress[0]) / 1024));
            progressBar.setMax((int) (totalSize / 1024));
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 30000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost httppost = new HttpPost(getString(R.string.alamat_server) + getString(R.string.link_upload_file));

            try
            {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener()
                        {
                            @Override
                            public void transferred(long num)
                            {
                                publishProgress((int) (num));
                            }
                        });

                File sourceFile = new File(filePath);

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
                String formattedDate = df.format(cal.getTime());
                entity.addPart("image", new FileBody(sourceFile));
                ff = SigapApp.getPgid() + "_" + formattedDate + ".jpg";
                entity.addPart("namafile", new StringBody(ff));
                entity.addPart("bagian", new StringBody("foto_pengguna"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200)
                {
                    responseString = EntityUtils.toString(r_entity);
                }
                else
                {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result)
        {
            try
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            new UpdatePenggunaManager(getApplicationContext()).execute(SigapApp.getPgid(), ff);
                        }
                        else if (query_result.equals("GAGAL"))
                        {
                            String pesan = jsonObj.getString("pesan");
                            Snackbar.make(TIFoto, "Pengiriman gagal, silakan coba lagi. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), result  + " - foto", Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (Exception e)
                    {
                        progressBar.dismiss();
                        String pesan = e.getMessage();
                        Snackbar.make(TIFoto, "Pengiriman gagal, periksa koneksi Anda. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
            }
            catch (Exception e)
            {
                progressBar.dismiss();
                String pesan = e.getMessage();
                Snackbar.make(TIFoto, "Terjadi kesalahan, silakan coba lagi. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
            super.onPostExecute(result);
        }
    }

    class UpdatePenggunaManager extends AsyncTask<String, Void, String>
    {
        private Context context;

        public UpdatePenggunaManager(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);
                data.put("foto", arg0[1]);

                link = getString(R.string.alamat_server) + getString(R.string.link_ubah_foto);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            Log.i("# # # ##  #", "onPostExecute: " + result);
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {

                    } else if (query_result.equals("GAGAL"))
                    {
                        Toast.makeText(context, "Pendaftaran gagal.", Toast.LENGTH_SHORT).show();
                    } else if (query_result.equals("EMAIL TIDAK VALID"))
                    {
                        Toast.makeText(context, "Email sudah dipakai.", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(context, "Tidak dapat terhubung ke database.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Toast.makeText(context, "Tidak dapat mengambil data JSON.", Toast.LENGTH_SHORT).show();
            }

            progressBar.dismiss();
        }
    }
}
