package com.sigapp.id;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Config;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.sigapp.id.App.AndroidMultiPartEntity;
import com.sigapp.id.App.RequestHandler;
import com.sigapp.id.App.SigapApp;
import com.sigapp.id.App.Validation;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class TambahLaporanActivity extends AppCompatActivity
{
    private ImageView IVFoto, BTambahLaporan, BAmbilLokasi;
    private VideoView VVideo;
    private static EditText TBJudul, TBIsi, TBLokasi;
    private ProgressDialog progressBar;
    private static String filePath = null, tahap, ff, lat, ling, alamat;
    private boolean isImage;
    long totalSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambahlaporan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        Intent i = getIntent();
        filePath = i.getStringExtra("filePath");
        isImage = i.getBooleanExtra("isImage", true);
        tahap = i.getStringExtra("tahap");

        IVFoto = (ImageView) findViewById(R.id.IVFoto);
        BTambahLaporan = (ImageView) findViewById(R.id.BTambahLaporan);
        BAmbilLokasi = (ImageView) findViewById(R.id.BAmbilLokasi);
        VVideo = (VideoView) findViewById(R.id.VVideo);
        TBJudul = (EditText) findViewById(R.id.TBJudul);
        TBIsi = (EditText) findViewById(R.id.TBIsi);
        TBLokasi = (EditText) findViewById(R.id.TBLokasi);

        if (filePath != null)
        {
            previewMedia(isImage);
        } else
        {
            Toast.makeText(getApplicationContext(), "File tidak ditemukan", Toast.LENGTH_LONG).show();
        }

        BTambahLaporan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (IsAllValid())
                {
                    new UploadFileToServer().execute();
                }
            }
        });

        BAmbilLokasi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), AmbilLokasiActivity.class);
                intent.putExtra("isdesa", false);
                startActivity(intent);
            }
        });
    }

    public static void AturLokasiDesa(String valamat, String vlat, String vling)
    {
        alamat = valamat;
        lat = vlat;
        ling = vling;

        if (!TextUtils.isEmpty(valamat))
        {
            TBLokasi.setText(valamat);
        }
        else
        {
            TBLokasi.setText("Lat.: " + lat + ", Ling.: " + ling);
        }
    }

    private boolean IsAllValid()
    {
        boolean valid = true;

        if (filePath == null)
        {
            Snackbar.make(IVFoto, "Sebagai bukti, kami memerlukan adanya foto.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBIsi.getText().toString()))
        {
            TBIsi.setError(getString(R.string.error_field_required));
            TBIsi.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBJudul.getText().toString()))
        {
            TBJudul.setError(getString(R.string.error_field_required));
            TBJudul.requestFocus();
            valid = false;
        }
        else if (TextUtils.isEmpty(TBLokasi.getText().toString()))
        {
            TBLokasi.setError(getString(R.string.error_field_required));
            TBLokasi.requestFocus();
            valid = false;
        }
        else if (!Validation.isSopan(TBIsi.getText().toString()))
        {
            TBIsi.setError(getString(R.string.error_tidak_sopan));
            TBIsi.requestFocus();
            valid = false;
        }

        return valid;
    }

    private void previewMedia(boolean isImage) {
        if (isImage)
        {
            IVFoto.setVisibility(View.VISIBLE);
            VVideo.setVisibility(View.GONE);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

            IVFoto.setImageBitmap(bitmap);
        }
        else
        {
            IVFoto.setVisibility(View.GONE);
            VVideo.setVisibility(View.VISIBLE);
            VVideo.setVideoPath(filePath);
            VVideo.start();
        }
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String>
    {
        @Override
        protected void onPreExecute()
        {
            progressBar = new ProgressDialog(TambahLaporanActivity.this);
            progressBar.setMessage("Mengirim...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setIndeterminate(false);
            progressBar.setCancelable(false);
            progressBar.show();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress)
        {
            progressBar.setProgress(progress[0]);
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(getString(R.string.alamat_server) + getString(R.string.link_upload_file));

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(filePath);

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
                String formattedDate = df.format(cal.getTime());
                entity.addPart("image", new FileBody(sourceFile));
                if (isImage)
                {
                    ff = SigapApp.getPgid() + "_" + formattedDate + ".jpg";
                }
                else
                {
                    ff = SigapApp.getPgid() + "_" + formattedDate + "." + filePath.substring(filePath.lastIndexOf(".") + 1);
                }
                entity.addPart("namafile", new StringBody(ff));
                entity.addPart("isimage", new StringBody(isImage ? "1" : "0"));
                entity.addPart("bagian", new StringBody("foto_laporan"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result)
        {
            try
            {
                String jsonStr = result;
                if (jsonStr != null)
                {
                    try
                    {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String query_result = jsonObj.getString("hasil");
                        if (query_result.equals("SUKSES"))
                        {
                            new TambahLaporanManager(getApplicationContext()).execute(SigapApp.getPgid(), tahap, TBJudul.getText().toString(), TBIsi.getText().toString(), lat, ling, alamat, ff, isImage ? "0" : "1");

                            Log.i("@%$^#@ (&^ *#", "onPostExecute: " + SigapApp.getPgid() + " # " + tahap + " # " + TBJudul.getText().toString() + " # " + TBIsi.getText().toString() + " # " + lat + " # " + ling + " # " + alamat + " # " + ff + " # " + String.valueOf(isImage ? "1" : "0"));
                        }
                        else if (query_result.equals("GAGAL"))
                        {
                            String pesan = jsonObj.getString("pesan");
                            Snackbar.make(BTambahLaporan, "Pengiriman gagal, silakan coba lagi. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), result  + " - foto", Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (Exception e)
                    {
                        progressBar.dismiss();
                        String pesan = e.getMessage();
                        Snackbar.make(BTambahLaporan, "Pengiriman gagal, periksa koneksi Anda. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
            }
            catch (Exception e)
            {
                progressBar.dismiss();
                String pesan = e.getMessage();
                Snackbar.make(BTambahLaporan, "Terjadi kesalahan, silakan coba lagi. " + pesan + " - foto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
            super.onPostExecute(result);
        }
    }

    class TambahLaporanManager extends AsyncTask<String, Void, String>
    {
        private Context context;

        public TambahLaporanManager(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0)
        {
            RequestHandler rh = new RequestHandler();

            String link;
            String result;

            try {
                HashMap<String,String> data = new HashMap<>();
                data.put("pgid", arg0[0]);
                data.put("tahap", arg0[1]);
                data.put("judul", arg0[2]);
                data.put("isi", arg0[3]);
                data.put("lat", arg0[4]);
                data.put("ling", arg0[5]);
                data.put("alamat", arg0[6]);
                data.put("namafile", arg0[7]);
                data.put("jenis", arg0[8]);

                link = getString(R.string.alamat_server) + getString(R.string.link_tambah_laporan);

                result = rh.sendPostRequest(link, data);

                return result;
            }
            catch (Exception e)
            {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            String jsonStr = result;
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("hasil");
                    if (query_result.equals("SUKSES"))
                    {
                        finish();
                    } else if (query_result.equals("GAGAL"))
                    {
                        Toast.makeText(context, "Pengiriman gagal.", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(context, "Tidak dapat terhubung ke database.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context, "Tidak dapat terhubung ke database, periksa koneksi internet kamu. " + jsonStr.toString(), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Toast.makeText(context, "Tidak dapat mengambil data JSON.", Toast.LENGTH_SHORT).show();
            }

            progressBar.dismiss();
        }
    }
}
