package com.sigapp.id;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sigapp.id.App.ImageHandler;
import com.sigapp.id.App.SigapApp;

public class EBooksActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private static ImageView mobil, background, background2;
    private static float nilaiakhir = 0, nilaiawal = 0;
    private static Activity cc;
    private boolean tutup;

    static boolean skip = false;
    private int halaman = 0;

    static ImageView b1 = null, b2 = null;

    boolean youtube = false;

    @Override
    public void finish()
    {
        skip = true;
        super.finish();
    }

    @Override
    protected void onStart()
    {
        skip = false;
        super.onStart();
    }

    public void Akhiri()
    {
        finish();

        if (SigapApp.getCurkuis().equalsIgnoreCase("20"))
        {
            Intent intent = new Intent(getApplicationContext(), PilihanActivity.class);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(getApplicationContext(), QuizActivity.class);
            startActivity(intent);
        }
    }

    public void MainkanAnimasi(final int halaman)
    {
        boolean geser = false;
        Log.i("HALAMAN", "MainkanAnimasi: " + String.valueOf(halaman));

        switch (halaman)
        {
            case 0:
                nilaiakhir = (SigapApp.getLebar_layar() / 10) + mobil.getLayoutParams().width;
                break;
            case 1:
                nilaiakhir = ((SigapApp.getLebar_layar() - (SigapApp.getLebar_layar() / 6)) - mobil.getLayoutParams().width) - SigapApp.getLebar_layar() / 10;
                break;
            case 2:
                geser = true;
                b1 = background;
                b2 = background2;
                nilaiawal = mobil.getTranslationX();
                nilaiakhir = SigapApp.getLebar_layar() / 8;
                break;
            case 3:
                nilaiakhir = SigapApp.getLebar_layar() - (SigapApp.getLebar_layar() / 8)  - mobil.getLayoutParams().width - nilaiakhir;
                //background.setImageBitmap(ImageHandler.decodeSampledBitmapFromResource(cc.getResources(), R.drawable.back_ebook3));
                background.setImageResource(R.drawable.back_ebook3);
                break;
            case 4:
                geser = true;
                b1 = background2;
                b2 = background;
                nilaiawal = mobil.getTranslationX();
                nilaiakhir = SigapApp.getLebar_layar() / 9;
                break;
            case 5:
                nilaiakhir = ((SigapApp.getLebar_layar() - (SigapApp.getLebar_layar() / 8)) - mobil.getLayoutParams().width) - nilaiakhir;
                //background2.setImageBitmap(ImageHandler.decodeSampledBitmapFromResource(cc.getResources(), R.drawable.back_ebook4));
                background2.setImageResource(R.drawable.back_ebook4);
                break;
            case 6:
                geser = true;
                b1 = background;
                b2 = background2;
                nilaiawal = mobil.getTranslationX();
                nilaiakhir = SigapApp.getLebar_layar() / 10;
                break;
            case 7:
                nilaiakhir = ((SigapApp.getLebar_layar() - (SigapApp.getLebar_layar() / 6)) - mobil.getLayoutParams().width) - nilaiakhir;
                //background.setImageBitmap(ImageHandler.decodeSampledBitmapFromResource(cc.getResources(), R.drawable.back_ebook5));
                background.setImageResource(R.drawable.back_ebook5);
                break;
            case 8:
                geser = true;
                b1 = background2;
                b2 = background;
                nilaiawal = mobil.getTranslationX();
                nilaiakhir = (SigapApp.getLebar_layar() / 5) - (mobil.getLayoutParams().width / 2);
                break;
            case 9:
                nilaiakhir = ((SigapApp.getLebar_layar() - (SigapApp.getLebar_layar() / 6)) - mobil.getLayoutParams().width) - nilaiakhir;
                break;
        }

        if (geser)
        {
            b1.animate().setDuration(2000).setStartDelay(1000).translationX(b1.getTranslationX()).translationXBy(b1.getTranslationX() - SigapApp.getLebar_layar()).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    super.onAnimationEnd(animation);
                }
            });

            mobil.animate().setDuration(2000).setStartDelay(1000).translationX(mobil.getTranslationX()).translationXBy(b1.getTranslationX() - SigapApp.getLebar_layar()).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    super.onAnimationEnd(animation);
                }
            });

            b2.animate().setDuration(2000).setStartDelay(1000).translationX(b2.getTranslationX()).translationXBy(b1.getTranslationX() - SigapApp.getLebar_layar()).setListener(new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    b1.setTranslationX(SigapApp.getLebar_layar());
                    mobil.setTranslationX(0 - mobil.getLayoutParams().width);

                    mobil.animate().setDuration(2500).translationX(mobil.getTranslationX()).translationXBy(nilaiakhir + mobil.getLayoutParams().width).setListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            if (halaman != 9)
                            {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        if (!skip && !tutup)
                                        {
                                            Intent intent = new Intent(cc, IsiEBooksActivity.class);
                                            intent.putExtra("halaman", halaman);
                                            cc.startActivity(intent);
                                            cc.overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
                                        }
                                        else if (tutup)
                                        {
                                            finish();
                                        }
                                    }
                                }, 800);
                            }
                            super.onAnimationEnd(animation);
                        }
                    });

                    super.onAnimationEnd(animation);
                }
            });
        }
        else
        {
            if (halaman != 9)
            {
                mobil.animate().setDuration(2500).setStartDelay(1000).translationX(mobil.getTranslationX()).translationXBy(nilaiakhir).setListener(new AnimatorListenerAdapter()
                {
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                if (!skip && !tutup)
                                {
                                    Intent intent = new Intent(cc, IsiEBooksActivity.class);
                                    intent.putExtra("halaman", halaman);
                                    cc.startActivity(intent);
                                    cc.overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
                                }
                                else if (tutup)
                                {
                                    finish();
                                }
                            }
                        }, 800);
                        super.onAnimationEnd(animation);
                    }
                });
            }
            else if (halaman == 9)
            {
                mobil.animate().setDuration(2500).setStartDelay(1000).translationX(mobil.getTranslationX()).translationXBy(nilaiakhir).setListener(new AnimatorListenerAdapter()
                {
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(EBooksActivity.this);
                                builder.setMessage("Silakan pilih bahasa dari video SIGAP REDD+ yang ingin Anda tonton.")
                                        .setCancelable(true)
                                        .setPositiveButton("Bahasa Indonesia", new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int did)
                                            {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=so5ybv8fRKs&feature=youtu.be")));
                                            }
                                        })
                                        .setNegativeButton("Language English", new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                                            {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=1dCE30j0hQU&feature=youtu.be")));
                                            }
                                        });
                                final AlertDialog alert = builder.create();
                                alert.show();
                                youtube = true;
                            }
                        }, 800);
                        super.onAnimationEnd(animation);
                    }
                });
            }
            else
            {
                mobil.animate().setDuration(3000).setStartDelay(1000).translationX(mobil.getTranslationX()).translationXBy(SigapApp.getLebar_layar()).setListener(new AnimatorListenerAdapter()
                {
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                skip = true;
                                Akhiri();
                            }
                        }, 800);
                        super.onAnimationEnd(animation);
                    }
                });
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebooks);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tutup = true;
                Toast.makeText(getApplicationContext(), "Tunggu sampai animasi selesai", Toast.LENGTH_LONG).show();
            }
        });

        mobil = (ImageView) findViewById(R.id.IVMobil);
        background = (ImageView) findViewById(R.id.IVBack);
        background2 = (ImageView) findViewById(R.id.IVBack2);

        background2.setTranslationX(SigapApp.getLebar_layar());

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(SigapApp.getLebar_layar() / 4, ViewGroup.LayoutParams.WRAP_CONTENT);
        mobil.setLayoutParams(layoutParams);

        mobil.setTranslationX(0 - mobil.getLayoutParams().width);

        cc = this;
    }

    @Override
    public void onBackPressed()
    {
        tutup = true;
        Toast.makeText(getApplicationContext(), "Tunggu sampai animasi selesai", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume()
    {
        Log.i("EBOOKS", "onResume: " + halaman);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {
                if (!youtube)
                {
                    MainkanAnimasi(halaman);
                    halaman++;
                }
                else
                {
                    skip = true;
                    Akhiri();
                }
            }
        }, 500);
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ebooks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_skip)
        {
            skip = true;
            Akhiri();
        }
        return super.onOptionsItemSelected(item);
    }
}
