package com.sigapp.id;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sigapp.id.App.Animasi;
import com.sigapp.id.App.SigapApp;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AmbilLokasiActivity extends AppCompatActivity implements OnMapReadyCallback
{
    String alamat, lat, ling;
    private GoogleMap mMap;
    Geocoder geocoder;

    private Button BPilihLokasi;
    private ImageView BCari;
    private EditText TBSearch;
    private TextView LAlamat;
    private boolean isDesa;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambil_lokasi);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        Intent intent = getIntent();
        isDesa = intent.getBooleanExtra("isdesa", true);

        LAlamat = (TextView) findViewById(R.id.LAlamat);
        BPilihLokasi = (Button) findViewById(R.id.BPilihLokasi);
        BCari = (ImageView) findViewById(R.id.BCari);
        TBSearch = (EditText) findViewById(R.id.TBSearch);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            geocoder = new Geocoder(this, Locale.forLanguageTag("ID"));
        } else
        {
            geocoder = new Geocoder(this, Locale.getDefault());
        }

        BPilihLokasi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isDesa)
                {
                    LoginActivity.AturLokasiDesa(alamat, lat, ling);
                    finish();
                }
                else
                {
                    TambahLaporanActivity.AturLokasiDesa(alamat, lat, ling);
                    finish();
                }
            }
        });

        BCari.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                List<Address> addresses;
                double latitude = 1, longitude = 1;
                try
                {
                    addresses = geocoder.getFromLocationName(TBSearch.getText().toString(), 1);
                    if (addresses.size() > 0)
                    {
                        latitude = addresses.get(0).getLatitude();
                        longitude = addresses.get(0).getLongitude();
                    }

                    mMap.clear();

                    LatLng lokasi = new LatLng(latitude, longitude);
                    mMap.addMarker(new MarkerOptions()
                            .position(lokasi)
                            .title(TBSearch.getText().toString())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker))
                            .flat(true));

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lokasi, 13));

                    if (BPilihLokasi.getAlpha() == 0)
                    {
                        BPilihLokasi.setAlpha(1);
                        Animasi.Translate(BPilihLokasi, 500, -50, "y");
                    }

                    lat = String.valueOf(latitude);
                    ling = String.valueOf(longitude);

                    new AmbilAlamat().execute(lokasi);
                } catch (IOException e)
                {
                }
            }
        });
    }

    private void buildAlertMessageNoGps()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("GPS di perangkat Anda tampaknya mati atau mode akurasi Lokasi Anda diset bukan Akurasi Tinggi, apakah Anda ingin mengaturnya ?")
                .setCancelable(true)
                .setPositiveButton("Iya", new DialogInterface.OnClickListener()
                {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                    {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener()
                {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                    {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener()
        {
            @Override
            public boolean onMyLocationButtonClick()
            {
                buildAlertMessageNoGps();
                return false;
            }
        });

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng latLng)
            {
                if (BPilihLokasi.getAlpha() == 0)
                {
                    BPilihLokasi.setAlpha(1);
                    Animasi.Translate(BPilihLokasi, 500, -50, "y");
                }

                lat = String.valueOf(latLng.latitude);
                ling = String.valueOf(latLng.longitude);
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("Pilih Lokasi Ini ?")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker))
                        .flat(true));
                new AmbilAlamat().execute(latLng);
            }
        });
    }

    class AmbilAlamat extends AsyncTask<LatLng, Void, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(LatLng... params) {
            LatLng location = params[0];

            Geocoder geocoder = new Geocoder(getBaseContext());
            String add = "";
            try
            {
                List<Address> addressList = geocoder.getFromLocation(location.latitude, location.longitude, 1);
                if (addressList.size() > 0)
                {
                    for (int i = 0; i < addressList.get(0).getMaxAddressLineIndex(); i++)
                    {
                        if (i == addressList.get(0).getMaxAddressLineIndex() - 1)
                        {
                            add += addressList.get(0).getAddressLine(i);
                        }
                        else
                        {
                            add += addressList.get(0).getAddressLine(i) + ", ";
                        }
                    }
                }
            }
            catch (IOException e)
            {
                return "EROR GAN";
            }
            return add;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            alamat = s;
            LAlamat.setText(s);
        }
    }
}
