package com.sigapp.id;

import android.animation.AnimatorInflater;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sigapp.id.App.Animasi;
import com.sigapp.id.App.SigapApp;

public class PlaySigapActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_sigap);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        ImageView BPlay = (ImageView) findViewById(R.id.BPlay);
        ImageView BExit = (ImageView) findViewById(R.id.BExit);
        ImageView BStart = (ImageView) findViewById(R.id.BStart);
        final LinearLayout LLSiap1 = (LinearLayout) findViewById(R.id.LLSiap1);
        final LinearLayout LLSiap2 = (LinearLayout) findViewById(R.id.LLSiap2);

        BPlay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Animasi.Fade(LLSiap1, LLSiap2, 250);
            }
        });

        BExit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        BStart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
                Intent intent;
                if (!SigapApp.getUsername().equalsIgnoreCase("Tidak Ada") && SigapApp.isCofas() && SigapApp.isDesa())
                {
                    intent = new Intent(getApplicationContext(), TahapActivity.class);
                }
                else if ((SigapApp.isCofas() == false || SigapApp.isDesa() == false) && !SigapApp.getUsername().equalsIgnoreCase("Tidak Ada"))
                {
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.putExtra("fungsi", "LENGKAPI");
                }
                else
                {
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                startActivity(intent);
                SigapApp.setTahap2("1");
                SigapApp.SimpanPengaturan();
            }
        });
    }
}
