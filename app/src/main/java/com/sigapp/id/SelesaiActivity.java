package com.sigapp.id;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sigapp.id.App.Animasi;

public class SelesaiActivity extends AppCompatActivity
{
    private TextView LWaktu, LPoint;
    private ScrollView SVMain;
    private RelativeLayout RLAnimasi;
    private ImageView BSelesai, IVBackView;

    public MediaPlayer mPlayer;

    private String waktu, point;

    private int[] back = {R.drawable.gambar_selesai1,
                            R.drawable.gambar_selesai2,
                            R.drawable.gambar_selesai3,
                            R.drawable.gambar_selesai4,
                            R.drawable.gambar_selesai5,
                            R.drawable.gambar_selesai6,
                            R.drawable.gambar_selesai7};

    private boolean mainkan = true;

    int in = 1;

    private void Animasikan()
    {
        if (mainkan)
        {
            Animasi.Fade(IVBackView, 1000, back[in], new AnimatorListenerAdapter()
            {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    in++;
                    if (in > (back.length - 1))
                    {
                        in = 0;
                    }
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            Animasikan();
                        }
                    }, 5000);
                    super.onAnimationEnd(animation);
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selesai);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mainkan = false;
                mPlayer.stop();
                finish();
            }
        });

        Intent intent = getIntent();
        waktu = intent.getStringExtra("waktu");
        point = intent.getStringExtra("point");

        LWaktu = (TextView) findViewById(R.id.LWaktu);
        LPoint = (TextView) findViewById(R.id.LPoint);
        SVMain = (ScrollView) findViewById(R.id.SVMain);
        RLAnimasi = (RelativeLayout) findViewById(R.id.RLAnimasi);
        BSelesai = (ImageView) findViewById(R.id.BSelesai);
        IVBackView = (ImageView) findViewById(R.id.IVBackView);

        mPlayer = MediaPlayer.create(SelesaiActivity.this, R.raw.music);
        mPlayer.setLooping(true);

        LWaktu.setText("Anda telah menyelesaikan seluruh tahapan dari SIGAP REDD+ dalam waktu " + waktu + " hari");
        LPoint.setText("Dan mengumpulkan point sebanyak " + point);

        BSelesai.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Animasi.Fade(SVMain, RLAnimasi, 200);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        Animasikan();
                    }
                }, 5000);
                mainkan = true;
                mPlayer.start();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        mainkan = false;
        mPlayer.stop();
        super.onBackPressed();
    }

    @Override
    protected void onPause()
    {
        mainkan = false;
        mPlayer.stop();
        super.onPause();
    }
}
